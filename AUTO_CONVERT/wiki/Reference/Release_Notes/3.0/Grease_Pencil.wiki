= Blender 3.0: Grease Pencil =

== Line Art ==
* Speed up tile access. ([https://projects.blender.org/blender/blender/commit/6ad4b8b7 6ad4b8b7])
* Better tolerance to faces perpendicular to view. ([https://projects.blender.org/blender/blender/commit/5b176b66 5b176b66])
* Threaded object loading. ([https://projects.blender.org/blender/blender/commit/1b07b7a0 1b07b7a0])
* Cached calculation for modifiers that are in the same stack. ([https://projects.blender.org/blender/blender/commit/247abdbf 247abdbf])
* Loose edge type and related chaining improvements. ([https://projects.blender.org/blender/blender/commit/841df831 841df831])
* Option to filter feature line using freestyle face mark. ([https://projects.blender.org/blender/blender/commit/3558bb8e 3558bb8e])
* Occlussion effectiveness support for mesh material. ([https://projects.blender.org/blender/blender/commit/cf21ba37 cf21ba37])
* Option to filter feature line with collection intersection masks. ([https://projects.blender.org/blender/blender/commit/d1e0059e d1e0059e])
* Camera overscan allows line art effective range to go beyond image frame, preventing strokes from ending right at the border. ([https://projects.blender.org/blender/blender/commit/8e9d06f5 8e9d06f5])
* Automatic crease with flat/smooth surfaces, which allows crease detection to follow smooth/flat shading and auto-smooth configuration. ([https://projects.blender.org/blender/blender/commit/c1cf66bff3c0 c1cf66bff3])
* Stroke offset function, allow strokes to be interacting with scene depth without showing in-front of everything.([https://projects.blender.org/blender/blender/commit/c3ef1c15 c3ef1c15])
* Custom camera allows line art to compute strokes from other cameras instead of the active one.([https://projects.blender.org/blender/blender/commit/efbd3642 efbd3642])
* Trimming edges right at the image border, useful when having multiple camera setup where you want the border to be clean.([https://projects.blender.org/blender/blender/commit/ec831ce5 ec831ce5])

== Operators ==
* New operator to bake GPencil transformed strokes into a new GPencil object. ([https://projects.blender.org/blender/blender/commit/06f86dd4d9a2 06f86dd4d9])
* Multiframe support added to Reproject operator. ([https://projects.blender.org/blender/blender/commit/e1acefd45e23 e1acefd45e])
* Multiframe support added to Move to Layer operator. ([https://projects.blender.org/blender/blender/commit/e4cebec647fb e4cebec647])
* Mask layer list now can be reordered.  ([https://projects.blender.org/blender/blender/commit/8032bd98d820 8032bd98d8])
* Now Blank objects include Layer and Material by default. ([https://projects.blender.org/blender/blender/commit/5cb1e18f7207 5cb1e18f72])
* Added support to convert Text to Grease Pencil object. ([https://projects.blender.org/blender/blender/commit/41820e8a8e70 41820e8a8e])
* New Bracket keymaps (`[`, `]`) to increase and decrease brush size. ([https://projects.blender.org/blender/blender/commit/c58bf31aeddd c58bf31aed])
* New operator to Copy materials to other grease pencil object. Can copy Active or All materials. ([https://projects.blender.org/blender/blender/commit/960535ddf344 960535ddf3]), ([https://projects.blender.org/blender/blender/commit/f6cb9433d45a f6cb9433d4])
* Layer Copy to Object has been renamed to Copy Layer to Object and allows to copy all layers at once. ([https://projects.blender.org/blender/blender/commit/960535ddf344 960535ddf3]), ([https://projects.blender.org/blender/blender/commit/f6cb9433d45a f6cb9433d4])
* New operator to normalize the Thickness or the Opacity of Strokes. ([https://projects.blender.org/blender/blender/commit/f944121700da f944121700])
* New Select Random operator. ([https://projects.blender.org/blender/blender/commit/a7aeec26550e a7aeec2655])
* New Convert Mesh to Grease Pencil copy the Vertex Groups with weights. ([https://projects.blender.org/blender/blender/commit/88dc274d0533 88dc274d05])
* Automerge when drawing strokes has been improved for better join when thickness is very different.  ([https://projects.blender.org/blender/blender/commit/ae334532cffb ae334532cf])

== Tools ==
* New `Dilate` parameter for Fill brush to expand filled area to fill small gaps. ([https://projects.blender.org/blender/blender/commit/f8f6e0b25621 f8f6e0b256])
* Annotations: Restore the `Placement` parameter in 2D Editors. ([https://projects.blender.org/blender/blender/commit/6ee14c966d0 6ee14c966d])

== UI ==
* The Topbar `Leak Size` parameter has been replaced by new `Dilate` parameter. `Leak Size` has been moved to Advanced panel. ([https://projects.blender.org/blender/blender/commit/f8f6e0b25621 f8f6e0b256])
* Removed duplicated `B` keymap to insert Blank keyframe in Drawing Mode. This was old keymap and it has been replaced with `I` menu. ([https://projects.blender.org/blender/blender/commit/04679794259f 0467979425])
* Annotations now display Stabilizer parameters in topbar and also in VSE. ([https://projects.blender.org/blender/blender/commit/0a83f32d79bc 0a83f32d79])
* New `Use Lights` option now available for when creating line art GPencil objects to allow convenient access to desired effect. ([https://projects.blender.org/blender/blender/commit/45b28c0f8847 45b28c0f88])

[[File:GP Use Lights.png|center]]

* New option in Topbar to define the type of caps (Rounded/Flat) used in new strokes. ([https://projects.blender.org/blender/blender/commit/c1730ed16568 c1730ed165])
* New keymap `Shift+Ctrl+M`to merge layer. ([https://projects.blender.org/blender/blender/commit/27b9cb7a1e67 27b9cb7a1e])
* Added buttons to move Up and Down Vertex Groups in the list. ([https://projects.blender.org/blender/blender/commit/2b64b4d90d67 2b64b4d90d])
* Use Scale Thickness option is now enabled by default in 2D template. ([https://projects.blender.org/blender/blender/commit/6fc92b296fae 6fc92b296f])
* List of modifiers reorganized moving some of them to new `Modify` column. ([https://projects.blender.org/blender/blender/commit/9cf593f30519 9cf593f305])

== Modifiers and VFX ==
* New Vertex Weight modifiers to generate weights base on Proximity or Angle on the fly to be used in any modifier. ([https://projects.blender.org/blender/blender/commit/29b65f534512 29b65f5345], [https://projects.blender.org/blender/blender/commit/368b56c9a132 368b56c9a1])
* New Randomize options for Offset modifier. ([https://projects.blender.org/blender/blender/commit/6a2bc40e0131 6a2bc40e01])
* Now Offset modifier uses the weights in the randomize parameters. ([https://projects.blender.org/blender/blender/commit/b73dc36859e0 b73dc36859])
* New Length modifier that allows modification to stroke length in percentage to its own length or in geometry space ([https://projects.blender.org/blender/blender/commit/cd1612376138 cd16123761]), also supports extending strokes based on curvature ([https://projects.blender.org/blender/blender/commit/25e548c96b3d8 25e548c96b]). 

[[File:Length Modifier Test.mp4|800px|center]]

[[File:GPencil Length.png|800px|center]]

* New Dot-dash modifier that allows generating dot-dash lines and assign different materials to each segments. ([https://projects.blender.org/blender/blender/commit/a2c5c2b4068d a2c5c2b406])

[[File:GPencil Dash.png|800px|center]]

== Compositing ==
* Added a new option to use masks during view layer render ([https://projects.blender.org/blender/blender/commit/e459a25e6cbe e459a25e6c]). This toggle can be found in the 
"Layers" -> "Relations" panel called "Use Masks in Render". The option is disabled if there is no view layer selected.
{|
|-
| [[File:GPencil disable masks viewlayer (not disabled).png|400px]] || [[File:GPencil disable masks viewlayer (disabled).png|400px]]
|}