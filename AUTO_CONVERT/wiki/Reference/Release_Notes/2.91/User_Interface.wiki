= Blender 2.91: User Interface =

== Outliner ==

* Collection colors: object collections now have a color tag, set from the right-click menu. ([https://projects.blender.org/blender/blender/commit/33d7b36cecd4 33d7b36cec]) ([https://projects.blender.org/blender/blender/commit/16f625ee654 16f625ee65]) ([https://projects.blender.org/blender/blender/commit/7b3d38a72d3 7b3d38a72d])

[[File:Outliner-colors.png|thumb|center]]

* Move mode toggling to the left column. When in non-object modes, icon buttons are shown in the left column for toggling modes. Clicking a dot icon will switch that object to become the new active object, ctrl-click adds the object into the current mode. ([https://projects.blender.org/blender/blender/commit/2110af20f5e6 2110af20f5])

* Add left and right walk navigation. When the active element is expanded, walking right will select the first child element. When the active is closed, walking left will select the parent element. ([https://projects.blender.org/blender/blender/commit/70151e41dc02 70151e41dc])
* The collection exclude checkbox was moved to the right with the other restriction toggles. ([https://projects.blender.org/blender/blender/commit/8bce181b71545 8bce181b71])
* Use shift-click on restrict icons for setting visibility and selectability of bone children to be consistent with objects and collections. ([https://projects.blender.org/blender/blender/commit/9de18c361bb4 9de18c361b])
* Drag & drop for modifiers, constraints and grease pencil effects. ([https://projects.blender.org/blender/blender/commit/1572da858df 1572da858d])
** Drag within the object or bone to reorder
** Drag to another object or bone to copy
** Drag the parent element (Modifiers, Constraints, or Effects) to link all to the target object or bone.
* Use the right-click target element to determine the type of context menu. For example, a right-click on a modifier will show the modifier menu even if objects are selected. ([https://projects.blender.org/blender/blender/commit/b0741e1dcbc5 b0741e1dcb])
* Display grease pencil modifiers and shader effects in the tree ([https://projects.blender.org/blender/blender/commit/2c34e09b08fb 2c34e09b08])
* Object data (can now be dropped into the 3D Viewport to create object instances), especially useful for orphan meshes, curves.. etc. ([https://projects.blender.org/blender/blender/commit/e56ff76db5b44ae6784a26ff6daf9864b2387712 e56ff76db5])
* Rename "Sequence" Display Mode to "Video Sequencer" ([https://projects.blender.org/blender/blender/commit/af843be3012 af843be301])

== Property Search ==
Search is now possible in the property editor, with automatically expanding and collapsing panels. `Ctrl-F` starts a search, and `alt-F` clears the search field. ([https://projects.blender.org/blender/blender/commit/bedbd8655ed1 bedbd8655e], [https://projects.blender.org/blender/blender/commit/8bcdcab659fb 8bcdcab659], [https://projects.blender.org/blender/blender/commit/7c633686e995 7c633686e9])

Tabs without search results will appear grayed out in the tab bar on the left. If a result is not found in the current tab, the search switches to the next one with a search result.

[[File:Property Search 2.91 Video.mp4|thumb|center|Searching across all tabs in the property editor]]


== Other ==
* Masking: Update the mask menu to be more consistent with other editors by rearranging and adding missing operators. ([https://projects.blender.org/blender/blender/commit/c598e939ad2 c598e939ad]), ([https://projects.blender.org/blender/blender/commit/4155d77026d 4155d77026]).
* Menus: Aesthetic tweaks to Select All by Type operator ([https://projects.blender.org/blender/blender/commit/8f4f9275cea 8f4f9275ce])
* UV Editor: Add missing Unwrapping tools to UV menu ([https://projects.blender.org/blender/blender/commit/4e3cb956385 4e3cb95638])
* 3D Viewport: Move Live Unwrap from UV menu to toolbar ([https://projects.blender.org/blender/blender/commit/637699e78ab 637699e78a])
* Many searches now support fuzzy and prefix matching. ([https://projects.blender.org/blender/blender/commit/98eb89be5dd08f3 98eb89be5d])
* Add temperature units option with support for Kelvin, Celsius and Fahrenheit. ([https://projects.blender.org/blender/blender/commit/36aeb0ec1e2e 36aeb0ec1e])
** Note that math operations are not supported for non-Kelvin temperature units, and will lead to incorrect results.
* Move curve geometry "start and end mapping" to a new subpanel. ([https://projects.blender.org/blender/blender/commit/4e667ecef92f 4e667ecef9])
* Auto keyframing settings are moved to their own popover in the timeline header. ([https://projects.blender.org/blender/blender/commit/099ce95ef36d 099ce95ef3])
[[File:Auto Keyframing Popover 2.91.png|thumb|center]]
* Option to invert filter in Dopesheet, Timeline and Curve Editor. ([https://projects.blender.org/blender/blender/commit/fecb276ef7b3 fecb276ef7])
* Change the invert list filter icon to be consistent with other areas. ([https://projects.blender.org/blender/blender/commit/459618d8606e 459618d860])
* "Reset to Default Value" in button right click menus now works in many more places, including modifiers and particle settings ([https://developer.blender.org/T80164#1034116 Reference])
* More information is now shown in tooltips in the "Open Recent Files" menu: file path, size, and modification date ([https://projects.blender.org/blender/blender/commit/53792e32e7dc 53792e32e7]).
[[File:Recent Details.png|thumb|center|600px]]
* Changes to "Quit" and "About" dialogs with change to monochrome versions of the large alert icons (and blender logo). ([https://projects.blender.org/blender/blender/commit/dc71ad062408 dc71ad0624]).
[[File:Monochrome Alerts.png|thumb|center|400px]]
* Float inputs should never show the confusing value of negative zero. ([https://projects.blender.org/blender/blender/commit/c0eb6faa47ec c0eb6faa47]).
* Categorized List Menus get slight change in format with nicer headings. ([https://projects.blender.org/blender/blender/commit/aa244a7a68db aa244a7a68]).
[[File:Categorized Menu.png|thumb|center|700px]]