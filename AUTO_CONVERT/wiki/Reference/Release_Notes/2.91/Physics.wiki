== Rigid Bodies ==
* Support for "Compound Shape" collisions ([https://projects.blender.org/blender/blender/commit/820ca419e098 820ca419e0]).
[[File:Chains comp.mp4|500px|thumb|center|Simulation in real time at 60fps]]

* You now can combine multiple primitive shapes into a concave shape. For instance, to a chain link collision shape you can use multiple wire edge child objects:

[[File:Chain link compound shapes.png|500px|thumb|center]]

* Doing it like this makes the simulation run a lot faster than if using the "mesh" collision shape. It is also in most cases a lot more stable.

== Fluid ==
* Improved obstacle handling:
** Advanced obstacle-liquid interaction (UI toggle "Fractional Obstacles") has been improved
** Particles now flow more smoothly over inclined obstacles
** For even finer control, there is a new option "Obstacle Distance" which defines the distance that liquid will maintain towards any obstacle.
[[File:Beach_2901.mp4|500px|thumb|center|Old simulation behavior (2.90): Some particles stick to obstacles]]
[[File:Beach_291.mp4|500px|thumb|center|New simulation behavior (2.91): More fluid flow along obstacles]]

* OpenVDB cache:
** In `.vdb` format, liquid simulations can now be saved with ''Full'' (32 bit), ''Half'' (16 bit) or ''Mini'' (mix of 8 bit and 16 bit) precision ([https://projects.blender.org/blender/blender/commit/2ec07dfa182d 2ec07dfa18], [https://projects.blender.org/blender/blender/commit/3d1b5e35bddb 3d1b5e35bd])
** Especially scenes with large amounts of liquid particles benefit from smaller cache file sizes as viewport performance increases significantly
** E.g. beach scene from above (~300.000 liquid particles): Before '''4.1 GB''', now (''Mini'' format) '''1.26 GB'''

* Improved "Viewport Display" options:
**''Note: This feature is aimed at developers and when a deeper inspection of the simulation is required.''
** It is now possible to visualize liquid simulations grids (before only smoke).
** Vector display options have been improved: Velocities and forces can now be viewed per axis (MAC visualization).
** An option to display gridlines for the underlying simulation grid has been added.
** Cell coloring options have been added: Obstacle, fluid and empty cells can be identified in the underlying grid.
** In-depth report: [https://wiki.blender.org/wiki/User:Sriharsha/GSoC2020/Final_Report Liquid Simulation Display Options] ([https://projects.blender.org/blender/blender/commit/f137022f9919 f137022f99])
<div><ul style="text-align: center; list-style: inside;"> 
<li style="display: inline-block; vertical-align: top;"> [[File:Fluid Levelset Representation.png|thumb|center|Visualization of a 2D slice of the fluid level-set representation in a liquid simulation. (''Display Thickness'' used: '''0.02''')]] </li>
<li style="display: inline-block; vertical-align: top;"> [[File:Final Fluid MAC Vectors.png|thumb|center|''MAC Grid'' visualization for a 2D slice of the velocity vector field in a liquid simulation.]] </li>
</ul></div>