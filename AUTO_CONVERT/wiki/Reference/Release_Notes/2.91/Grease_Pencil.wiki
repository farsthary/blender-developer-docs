= Blender 2.91: Grease Pencil =

== User Interface ==
* Renamed Boundary Fill `Default` to `All`.  ([https://projects.blender.org/blender/blender/commit/fff12be9456c fff12be945])
* Boundary strokes are now visible, except in render mode. This was removed by error in the refactor. ([https://projects.blender.org/blender/blender/commit/ebf5ff8afb9a ebf5ff8afb])
* Brush Advanced panel now has `Ignore Transparent` and `Threshold` parameters related. ([https://projects.blender.org/blender/blender/commit/9e644b98a6f7 9e644b98a6])
* Set Onion Keyframe mode to `All` by default. Also renamed option from `All Types` to `All`. ([https://projects.blender.org/blender/blender/commit/3e56dd8fd921 3e56dd8fd9])
* Replace Interpolate shortcut to `Ctrl+E` instead of `Ctrl+Alt+E`. ([https://projects.blender.org/blender/blender/commit/ee49ce482a79 ee49ce482a])
* New Subdivision parameter for Primitives in Topbar. ([https://projects.blender.org/blender/blender/commit/90baead9792b 90baead979])

== Operators ==
* New Cleanup Frames operator.  ([https://projects.blender.org/blender/blender/commit/382b9007f8f3 382b9007f8])
* New Step parameter for Interpolate Sequence.  ([https://projects.blender.org/blender/blender/commit/97871e16ff45 97871e16ff])
* The convert mesh to strokes and bake mesh animation, now create layers and materials using the original object name. Also fixed problems when convert or bake animation for  several objects at the same time. ([https://projects.blender.org/blender/blender/commit/256b59b76f0a 256b59b76f])
* New material option to set as Holdout. This allows to open holes in filled areas. This option only works when the stroke uses Material mode. ([https://projects.blender.org/blender/blender/commit/0335c1fd21dd 0335c1fd21])

[[File:Gpencil_holdout.mp4|600px|thumb|center|Holdout Material]]

* New Trace Images operator using Potrace library. ([https://projects.blender.org/blender/blender/commit/4d62bb8fe57c 4d62bb8fe5])

[[File:TraceGP.mp4|600px|thumb|center|Trace Image]]

This functionality is designed to trace a black and white image into grease pencil strokes. If the image is not B/W, the trace try to convert to bitone image. For better results, use manually converted to black and white images and try to keep the resolution of the image small. High resolutions could produce very dense strokes.

== Tools ==
* Draw Brushes: New predefined mode.  ([https://projects.blender.org/blender/blender/commit/7becd283cc27 7becd283cc])
* Fill Brush: New Layer modes. ([https://projects.blender.org/blender/blender/commit/8fc7c3539ade 8fc7c3539a])
* Fill Brush: Fill inverted pressing `Ctrl` key. Also added new direction buttons for Fill in topbar. ([https://projects.blender.org/blender/blender/commit/4ada2909566c 4ada290956])

== Modifiers and VFX ==
* Offset modifier scale thickness when modify scale.  ([https://projects.blender.org/blender/blender/commit/a0b0a47d8104 a0b0a47d81])