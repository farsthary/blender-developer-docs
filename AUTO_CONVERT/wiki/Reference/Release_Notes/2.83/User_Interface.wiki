= Blender 2.83: User Interface =

== Animation Editors ==

* Improved graph editor channels drawing ([https://projects.blender.org/blender/blender/commit/4aa0e21 4aa0e21])
* Improved graph and NLA editor sidebar panel layout ([https://projects.blender.org/blender/blender/commit/04e318de3a4 04e318de3a])
* Rename 'View Frame' to 'Go to Current Frame' ([https://projects.blender.org/blender/blender/commit/dee01cad19f dee01cad19])
* Change 'Lock Time to Other Windows' > 'Sync Visible Range' and add to Sequencer. ([https://projects.blender.org/blender/blender/commit/fe772bf8186 fe772bf818])
* Changed dopesheet & timeline filter names ([https://projects.blender.org/blender/blender/commit/9ccc73ade8a25db5bb 9ccc73ade8]):
** Only Selected → Only Show Selected
** Show Errors → Only Show Errors
** Display Hidden → Show Hidden
* Show animation cancel button in all status-bars ([https://projects.blender.org/blender/blender/commit/25cb12dc71e 25cb12dc71])

== File Browser ==

* Support for file attributes and hidden files. ([https://projects.blender.org/blender/blender/commit/1fb62d1272db 1fb62d1272])
* Show icons for special folder locations. ([https://projects.blender.org/blender/blender/commit/84c9a99cca90 84c9a99cca]) ([https://projects.blender.org/blender/blender/commit/1af8e0cc6c47 1af8e0cc6c]) ([https://projects.blender.org/blender/blender/commit/a622e29a2541 a622e29a25])
* Spport for Ctrl+F shortcut to start filtering files. ([https://projects.blender.org/blender/blender/commit/e8ab0137f876 e8ab0137f8])
* Recognizes .fountain files as text files. ([https://projects.blender.org/blender/blender/commit/aa4579c35f93 aa4579c35f])

== Miscellaneous ==

* Improve toolbar width snapping ([https://projects.blender.org/blender/blender/commit/ac7eb710890 ac7eb71089])
* Consolidate masking-related brush controls ([https://projects.blender.org/blender/blender/commit/c01246f6c0f c01246f6c0])
* Change Area Duplicate Icon ([https://projects.blender.org/blender/blender/commit/452834f1e3a 452834f1e3])
* On Windows, restore app from minimized when closing from taskbar ([https://projects.blender.org/blender/blender/commit/22ca8b8aee99 22ca8b8aee])
* Dynamically enable and disable Edit Menu items based on whether currently applicable ([https://projects.blender.org/blender/blender/commit/b7075049732a b707504973])
* Visual changes to Info Editor ([https://projects.blender.org/blender/blender/commit/aa919f3e8202 aa919f3e82])
* Improved keyboard symbols for menus and status bar, following platform conventions. ([https://projects.blender.org/blender/blender/commit/dc3f073d1c52 dc3f073d1c]) ([https://projects.blender.org/blender/blender/commit/f051d47cdbce f051d47cdb])
* Overflowing text lines now use ellipsis character to indicate line continuation. ([https://projects.blender.org/blender/blender/commit/63d5b974ccfa 63d5b974cc])
* Larger Alert icons for dialogs ([https://projects.blender.org/blender/blender/commit/a210b8297f5a a210b8297f])
* Language Selection on "Quick Start" Splash screen ([https://projects.blender.org/blender/blender/commit/a210b8297f5a a210b8297f])
* Improved UI Layout for 3D Mouse Settings, all settings accessible from Preferences ([https://projects.blender.org/blender/blender/commit/ce0db0d329985e4 ce0db0d329])
* Keep outliner selection in sync for more operators ([https://projects.blender.org/blender/blender/commit/5d14463e1aee 5d14463e1a]) ([https://projects.blender.org/blender/blender/commit/c06a40006d6c c06a40006d]) ([https://projects.blender.org/blender/blender/commit/f772a4b8fa87 f772a4b8fa])
* Display and allow creation of drivers and keyframes for bone visibility in the outliner ([https://projects.blender.org/blender/blender/commit/6f8d99322cf6 6f8d99322c])