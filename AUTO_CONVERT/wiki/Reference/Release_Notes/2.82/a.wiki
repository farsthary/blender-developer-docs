== Blender 2.82a: Bug Fixes ==

=== Blender ===

* Fix [http://developer.blender.org/T74003 #74003]: Autocomplete bug with mesh.loop_triangles. in Blender Python Console. ([https://projects.blender.org/blender/blender/commit/69c587888  rB69c58788])
* Fix [http://developer.blender.org/T73898 #73898]: UDIM crash changing form tiled to single ([https://projects.blender.org/blender/blender/commit/d6977b5  rBd6977b5])
* Fix [http://developer.blender.org/T73862 #73862]: Fluid Sim file handle resource leak ([https://projects.blender.org/blender/blender/commit/60890cc  rB60890cc])
* Fix [http://developer.blender.org/T74182 #74182]: Crash saving images from non-image spaces ([https://projects.blender.org/blender/blender/commit/096936fe1  rB096936fe])
* Fix [http://developer.blender.org/T72903 #72903]: Bone envelope & bone size tool functionality swapped ([https://projects.blender.org/blender/blender/commit/dd2cdd436  rBdd2cdd43])
* Fix [http://developer.blender.org/T74278 #74278]: camera border gizmo size ([https://projects.blender.org/blender/blender/commit/9cac5fa681  rB9cac5fa6], [https://projects.blender.org/blender/blender/commit/360443a48  rB360443a4])
* Fix [http://developer.blender.org/T54270 #54270]: Reset last_hit and last_location when reading the file ([https://projects.blender.org/blender/blender/commit/2df040ed58fb  rB2df040ed])
* Fix [http://developer.blender.org/T74431 #74431]: crash when compiling renderpass shader on some AMD drivers ([https://projects.blender.org/blender/blender/commit/9c4523b1fde4  rB9c4523b1] + [https://projects.blender.org/blender/blender/commit/e5f98c79b0ed  rBe5f98c79])
* Fix [http://developer.blender.org/T74295 #74295]: Cloth + Internal springs crashes on a non-polygonal geometry ([https://projects.blender.org/blender/blender/commit/1648a7903672  rB1648a790])
* Fix Fix (unreported) Separate bones creates empty armature ([https://projects.blender.org/blender/blender/commit/498397f7bd8  rB498397f7])
* Revert "Constraints: remove special meaning of Local Space for parentless Objects." ([https://projects.blender.org/blender/blender/commit/f881162f81e  rBf881162f])
* Fix [http://developer.blender.org/T73932 #73932]: modifying keyframes in nodes fails when there is an image sequence ([https://projects.blender.org/blender/blender/commit/f0a22f5  rBf0a22f5])
* Fix crash loading .blend file saved with Blender 2.25 ([https://projects.blender.org/blender/blender/commit/ed8aa15  rBed8aa15])
* Fix potential crash with Eevee render of missing image textures ([https://projects.blender.org/blender/blender/commit/ab18dbb  rBab18dbb])
* Fix [http://developer.blender.org/T72253 #72253]: Mantaflow adaptive domain cuts off ([https://projects.blender.org/blender/blender/commit/32fc22db5679  rB32fc22db])
* Keymap: Add front/back Alt-MMB absolute view axis switching ([https://projects.blender.org/blender/blender/commit/a200986273  rBa2009862])
* Fix [http://developer.blender.org/T72028 #72028]: Crash switching to vertex paint ([https://projects.blender.org/blender/blender/commit/31aefdeec5  rB31aefdee])
* Fix bone envelopes displaying wrong when armature is scaled ([https://projects.blender.org/blender/blender/commit/ee7034949fd  rBee703494])
* Fix Vertex weight gradient tool show wrong weight/strength values in the UI ([https://projects.blender.org/blender/blender/commit/f38c54d56e  rBf38c54d5])
* Fix [http://developer.blender.org/T74225 #74225]: Image (from sequence) cannot be loaded ([https://projects.blender.org/blender/blender/commit/9dbfc7ca8bf  rB9dbfc7ca])
* Fix [http://developer.blender.org/T63892 #63892]: Tools cannot be registered into some contexts (e.g. PAINT_TEXTURE) ([https://projects.blender.org/blender/blender/commit/d95e9c7cf8  rBd95e9c7c])
* Fix [http://developer.blender.org/T73369 #73369]: corner pin & sun-beam nodes gizmos are too big ([https://projects.blender.org/blender/blender/commit/212660f467  rB212660f4])
* Fix [http://developer.blender.org/T74425 #74425]: Cannot texture paint an images sequence anymore ([https://projects.blender.org/blender/blender/commit/ca717f0489  rBca717f04])

=== Add-ons ===
* glTF: Fix some strange reference error in blender api when exporting shapekeys / ApplyModifier ([https://projects.blender.org/blender/blender-addons/commit/659c121a68  rBA659c121])
* glTF: Fix crash using compositor rendering for image generation ([https://projects.blender.org/blender/blender-addons/commit/af687f5a041ee  rBAaf687f5])