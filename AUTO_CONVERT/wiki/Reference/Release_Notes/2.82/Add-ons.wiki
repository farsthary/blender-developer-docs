= Blender 2.82: Add-ons =

= Addons Updates =

== Rigify ==

* Some generation settings are stored in the metarig now, instead of being global to the file. ([https://projects.blender.org/blender/blender-addons/commit/71565f82  rBA71565f8])
* The `spines.basic_spine` rig now has new optional FK controls. ([https://projects.blender.org/blender/blender-addons/commit/dd723151  rBAdd72315])
* Spine and limbs now have an option (off by default) to create an arbitrarily movable rotation pivot control, similar to BlenRig. ([https://projects.blender.org/blender/blender-addons/commit/358bc43e  rBA358bc43])
* A new `basic.pivot` rig can be used as a parent of the spine to add a custom pivot between root and torso. ([https://projects.blender.org/blender/blender-addons/commit/358bc43e  rBA358bc43])
* The default rotation pivot of the foot IK control has been switched from toe to ankle by default, but can be switched back. ([https://projects.blender.org/blender/blender-addons/commit/c871246f  rBAc871246])
* The Human metarig now uses `super_finger` instead of `simple_tentacle`; the finger rig has new B-Bone and layer options. ([https://projects.blender.org/blender/blender-addons/commit/b9a821bf  rBAb9a821b])
* The arm rig can optionally generate an extra wrist pivot control for the IK hand, most useful for implementing finger IK. ([https://projects.blender.org/blender/blender-addons/commit/db3e15be  rBAdb3e15b], [https://projects.blender.org/blender/blender-addons/commit/7b529777  rBA7b52977])
* New `basic.raw_copy` rig that copies a bone without adding the ORG prefix, allowing copying complex ad-hoc rig setups verbatim. ([https://projects.blender.org/blender/blender-addons/commit/a50e87484  rBAa50e874])
* The `super_copy` and `raw_copy` rigs now have options to patch the parent and constraint targets after bones are generated, for advanced ad-hoc rigging. ([https://projects.blender.org/blender/blender-addons/commit/feb02092  rBAfeb0209])
* The `super_finger` rig now has optional experimental simple fingertip IK support, similar to some other rigs like BlenRig. ([https://projects.blender.org/blender/blender-addons/commit/a41ceb3a  rBAa41ceb3])

== Add Mesh Extra Objects ==

* New Rock Generator addon 
* The rock generator addon has been in addons contrib and nightly builds for several years now.
* Integrated now into the "Extra Objects" menu, more people can now get to appreciate Paul Marshall's work.

== Mesh Tools ==

* Mesh Tools has integrated the Mesh Relax addon into it's family.

== Viewport Pies ==

* New Ctrl/U Save Defaults and Preferences pie. Working across all editors, save your settings from the 1 menu.
* Returned: Proportional Edit pie. Rewritten by request. Proportional Edit pie offers an alternative to the built in Shift/O proportional menu.

== BlenderKit ==

* Advanced search options have been introduced for models and materials
* UI has been reworked, so now the add-on can be found in the top 3d view header. 
* Asset Bar now opens automatically after search ends, and can also be open after blender starts. This is a big speedup in interaction.
* An empty search searches the latest additions in the database.
* Material thumbnail generator has been improved.
[[File:Advanced material search - procedural.gif|frame|center|BlenderKit Procedural/Texture based material search]]]

= New Addons =

== Import Palettes ==

* This wonderful addition imports Photoshop and Krita palette files. From Antionio Vazquez.

== PDT ==

* Precision Drawing Tools from Alan Odom and Rune Morling.
* This great new addon introduces some CAD style techniques to the Blender toolkit.

== Add Node Presets ==

* This addon by Campbell Barton is promoted from addons contrib in nightly builds to main releases.
* In the expanded addons activation box, select a directory that has .blend files with node groups.
* You will now have a new Templates menu containing your nodes in the  Add > Node menu across the node editors.

== Collection Manager ==

* Manage Your Collections in the 3d Viewport. Thanks to Ryan Inch

== Sun Position ==

* Use real world coordinates to adjust your light to match the sun position at any time of day!

== Amaranth Toolkit ==

* Promoted from nightly builds to release with the thanks to Pablo Vazquez and CansecoGPC
* this addon provides many small features and functions to extend usability.