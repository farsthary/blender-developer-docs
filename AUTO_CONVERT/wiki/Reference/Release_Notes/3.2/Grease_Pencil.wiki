= Grease Pencil =

== Operators ==
* Improved smooth algorithm. ([https://projects.blender.org/blender/blender/commit/d4e1458db3a0 d4e1458db3])

== UI ==
* Scale Stroke Thickness is now visible in Pie menu. ([https://projects.blender.org/blender/blender/commit/acd7a648b17a acd7a648b1])

[[File:Pie Menu 1.png|800px|Pie Menu]]

== Modifiers and VFX ==
* New modifier: Envelope ([https://projects.blender.org/blender/blender/commit/cee6af00567f cee6af0056])

[[File:Envelope Modifier.png|800px]]

* Dot Dash modifier: 
** Lowered the bounds limits for dash and gap ([https://projects.blender.org/blender/blender/commit/8d4244691a2d 8d4244691a])

[[File:Dot dash.png|800px]]

** New option to use Cyclic per segment ([https://projects.blender.org/blender/blender/commit/97f2210157db 97f2210157]) 

[[File:Dot dash 04.png|800px|Dot dash Cyclic]]

* Smooth Modifier: New option to keep the overall shape closer to the original, avoiding the strokes to shrink ([https://projects.blender.org/blender/blender/commit/d4e1458db3a0 d4e1458db3])

[[File:Smooth Modifier.png|800px]]

* Build modifier: 
** New Additive mode. ([https://projects.blender.org/blender/blender/commit/e74838d0d011 e74838d0d0])
** Adds fade support ([https://projects.blender.org/blender/blender/commit/c4e4924096d1 c4e4924096])
** Adds origin object for the building process ([https://projects.blender.org/blender/blender/commit/c4e4924096d1 c4e4924096])

[[File:Build Modifier.png|800px|Build Modifier]]

== Performance ==
* Added a cache to speed up Grease Pencil object evaluation. This is currently used only for drawing strokes and sculpting using some brushes like the push brush. ([https://projects.blender.org/blender/blender/commit/e2befa425a84 e2befa425a])
* Improve multi-user Grease Pencil data performance with modifiers. ([https://projects.blender.org/blender/blender/commit/7ca13eef7c33 7ca13eef7c])

== Bug Fix ==
* Grease Pencil edit mode overlays now support the clipping region. ([https://projects.blender.org/blender/blender/commit/69a43069e857 69a43069e8])