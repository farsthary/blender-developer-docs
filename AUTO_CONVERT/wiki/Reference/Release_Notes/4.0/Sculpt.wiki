= Sculpt, Paint, Texture =
== Enhancements ==
* Vertex Paint: Add set vertex colors option to lock alpha ([https://projects.blender.org/blender/blender/pulls/111002 PR #111002])
* Sculpt: Remove dynamic topology "Smooth Shading" option ([https://projects.blender.org/blender/blender/pulls/110548 PR #110548])
* Sculpt: Expand Mask inverted fill ([https://projects.blender.org/blender/blender/pulls/111665 PR #111665])
* Weight Paint: Allow setting weights (Ctrl+X) without paintmask enabled ([https://projects.blender.org/blender/blender/commit/0b1d2d63fe 0b1d2d63fe])