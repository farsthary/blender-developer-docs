= Image editor =

* Image editor is able to handle large images. ([https://projects.blender.org/blender/blender/commit/bdd74e1e9338 bdd74e1e93])
* Tearing artifacts have been fixed on selected platforms. ([https://projects.blender.org/blender/blender/commit/bdd74e1e9338 bdd74e1e93])