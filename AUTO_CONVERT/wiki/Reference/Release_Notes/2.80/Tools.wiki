= Blender 2.80: Tools & Gizmos =

The 3D viewport and UV editor have new interactive tools and gizmos, along with a new toolbar. These make it easier to for new users to start using Blender, and for existing users to discover and use tools that previously required obscure key combinations.

[[File:Blender2.8_toolbar_config.png|600px|thumb|center|Toolbar in compact and wide configurations]]

== Toolbar ==

A new toolbar is displayed on the side of the 3D viewport and UV editor. When activating a tool in the toolbar, it stays active and shows relevant gizmos in the viewport. This is different than commands found in menus, which have a more immediate effect and whose settings can be adjusted after completing the operation.

For operations like moving or extruding, the user can choose to use either existing fast commands and keyboard shortcuts, or to use the toolbar instead. The manipulator system has been replaced by move, rotate and scale gizmos from the corresponding tools in the toolbar. For combining Move, Rotate or Scale gizmos, we now have the Transform tool. 

Some examples of the new interactive tools in action:
<center>
{|
 |valign=top|[[File:Blender2.8 tools edit mode tools.gif|600px|center]]
 |-
 |valign=top|[[File:Blender2.8 tools rotate tool .gif|600px|center]]
 |-
 |valign=top|[[File:Blender2.8 tools spin tool.gif|600px|center]]
 |-
 |valign=top|[[File:Blender2.8 tools spin tool 2.gif|600px|center]]
 |}
</center>

== Gizmos ==

Besides gizmos for tools, various elements like lights, camera, and the compositing backdrop image now have handles to adjust their shape or other attributes.

[[File:Blender2.8 tools lamp widget.gif|600px|thumb|center|Spot light gizmo]]