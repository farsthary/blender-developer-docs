= Blender 2.80: Preferences API =

What was formally named ''User Preferences'', is now just ''Preferences''. The Python API was tweaked accordingly:

{| class="wikitable"
|-
! scope="col" | Old !! New
|-
| <code>bpy.context.user_preferences</code> || <code>bpy.context.preferences</code>
|}

The Python API was also updated to be (mostly) synced with changes in the placement of options within the UI. The following members of <code>bpy.context.preferences</code> were changed:

{| class="wikitable"
|-
! scope="col" | Old !! New
|-
| <code>system.use_weight_color_range</code> || <code>view.use_weight_color_range</code>
|-
| <code>system.weight_color_range</code> || <code>view.weight_color_range</code>
|-
| <code>system.color_picker_type</code> || <code>view.color_picker_type</code>
|-
| <code>view.use_quit_dialog</code> || <code>view.use_save_prompt</code>
|-
| <code>view.use_mouse_depth_navigate</code> || <code>input.use_mouse_depth_navigate</code>
|-
| <code>view.use_mouse_depth_cursor</code> || <code>input.use_mouse_depth_cursor</code>
|-
| <code>view.use_cursor_lock_adjust</code> || <code>input.use_cursor_lock_adjust</code>
|-
| <code>view.use_camera_lock_parent</code> || <code>input.use_camera_lock_parent</code>
|-
| <code>view.use_zoom_to_mouse</code> || <code>input.use_zoom_to_mouse</code>
|-
| <code>view.use_auto_perspective</code> || <code>input.use_auto_perspective</code>
|-
| <code>view.use_rotate_around_active</code> || <code>input.use_rotate_around_active</code>
|-
| <code>system.use_text_antialiasing</code> || <code>view.use_text_antialiasing</code>
|-
| <code>system.text_hinting</code> || <code>view.text_hinting</code>
|-
| <code>system.font_path_ui</code> || <code>view.font_path_ui</code>
|-
| <code>system.font_path_ui_mono</code> || <code>view.font_path_ui_mono</code>
|-
| <code>system.use_international_fonts</code> || <code>view.use_international_fonts</code>
|-
| <code>system.language</code> || <code>view.language</code>
|-
| <code>system.use_translate_tooltips</code> || <code>view.use_translate_tooltips</code>
|-
| <code>system.use_translate_interface</code> || <code>view.use_translate_interface</code>
|-
| <code>system.use_translate_new_dataname</code> || <code>view.use_translate_new_dataname</code>
|-
| <code>input.show_ui_keyconfig</code> || <code>keymap.show_ui_keyconfig</code>
|-
| <code>input.active_keyconfig</code> || <code>keymap.active_keyconfig</code>
|-
| <code>system.author</code> || <code>filepaths.author</code>
|-
| <code>system.use_scripts_auto_execute</code> || <code>filepaths.use_scripts_auto_execute</code>
|-
| <code>system.use_tabs_as_spaces</code> || <code>filepaths.use_tabs_as_spaces</code>
|}

Note that even though the undo settings were moved to a different section too, decision was to keep their Python paths untouched. Reason being that we didn't want to break compatibility of such an often used API after the Beta release.