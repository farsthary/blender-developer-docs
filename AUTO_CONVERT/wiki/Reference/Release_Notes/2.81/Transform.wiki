= Blender 2.81: Transform & Snapping =

== Transform ==

=== Affect Origins ===

It's now possible to move object origins directly.

This is an alternative method of adjusting the origin, previously only possible placing the cursor and setting an objects origins to the cursor.

This only works for objects with data which can be transformed.

This is accessible from the Options panel.

=== Affect Parents (Skip Children) ===

It's now possible to transform parents without affecting their children.

This is accessible from the Options panel.

=== Mirror ===

Mirroring over the Y and Z axis is now supported, along with the existing X axis option. Multiple axes can also be enabled simultaneously. ([https://projects.blender.org/blender/blender/commit/3bd4f22 3bd4f22])

[[File:Transform mirror.png|600px|thumb|center|Mirror now has X, Y and Z options]]

== Snap ==

=== Edge Snapping Options ===

Two new snap options have been added:

* Edge Center, to snap to the middle of an edge. ([https://projects.blender.org/blender/blender/commit/5834124 5834124]);
* Edge Perpendicular, to snap to the nearest point on an edge. ([https://projects.blender.org/blender/blender/commit/dd08d68 dd08d68]);

<center>
{| class="transparent" style="text-align: center"
 |valign=top|[[File:Snap Edge Center and Perpendicular.png|293px|thumb|center|Snap modes Edge Center and Perpendicular]]
 |valign=top|[[File:Snap Perpendicular.png|300px|thumb|center|Edge Perpendicular snapping]]
 |}
</center>

=== Auto Merge ===

When auto merging vertices, adjacent edges and faces can now be automatically split to avoid overlapping geometry. This is controlled by the new "Split Edges & Faces" option for Auto Merge. ([https://projects.blender.org/blender/blender/commit/6b189d2 6b189d2])

[[File:Blender2.81_auto_merge_split.png|600px|thumb|center|Auto merge and split faces]]