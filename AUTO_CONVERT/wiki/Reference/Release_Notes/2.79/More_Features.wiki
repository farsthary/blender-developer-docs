= Blender 2.79: More Features =

== Video Encoding ==

[[Image:Release_notes_279_video_overhaul.png|frame|The new Encoding panel.]]

Video encoding settings have been simplified, along with the addition of a Constant Rate Factor (CRF) mode. ([https://projects.blender.org/blender/blender/commit/a7e74791221e a7e7479122])

* Clearer separation between container and codec. The "format" label is no longer used, as this is too ambiguous (both container and codec could be called "format"). As a result, codecs were removed from the container list.
* Added Constant Rate Factor (CRF) mode, which allows changing the bit-rate depending on the desired quality and the input. This generally produces the best quality videos in a single pass, at the expense of not knowing the exact bit-rate and file size.
* Added FFmpeg speed presets, ranging from "Very slow" (but best compression) to "Ultra fast" (but bigger file). By default the "Medium" preset is used.
* Added optional maximum of non-B-frames between B-frames (max_b_frames).
* Presets were adjusted for these changes, and new presets added. One of the new presets is recommended for reviewing videos, as it allows players to scrub through it easily. This preset also requires control over the max_b_frames setting.
* More modern defaults. Rather than defaulting to the old MPEG2 tailored for DVD, Blender now defaults to h.264 inside a Matroska (MKV) container. Your existing startup file may still be set to MPEG2, though, so if you want to use this as a default, right-click on the container, codec, and output quality options, and choose "Reset to Default Value".


User interface changes:
* Renamed "MPEG" in the output file format menu with "FFmpeg video", as this is more accurate. After all, when this option is chosen FFmpeg is used, which can also output non-MPEG files.
* Certain parts of the interface are disabled when not in use:
** Video bit rate options are not used when a constant rate factor is given.
** Audio bitrate & volume are not used when no audio is exported.


Support:
* QuickTime format is disabled since the build options have been fully removed in macOS 10.12. ([https://projects.blender.org/blender/blender/commit/521b98157568 521b981575])

<br style="clear:both;">

== Image Sequences ==

Previously, when selecting more than one image file for Open Image operator (from Image Editor e.g.), Blender would try to detect automatically sequences of animated images, based on numbers in file names. This is handy, but also gets in the way when you do want to get single images. That auto-detection of sequences can now be prevented by disabling "Detect Sequences" operator option (which is enabled by default).

== 3D Viewport ==

The Blender render and GLSL viewport now supports more shading features to match each other:
* Object Info node support for GLSL viewport and render. ([https://projects.blender.org/blender/blender/commit/78b5d66af 78b5d66af])
* Layer Weight node support for render. ([https://projects.blender.org/blender/blender/commit/12e1732f 12e1732f])
* Fresnel node support for render. ([https://projects.blender.org/blender/blender/commit/4f03d32 4f03d32])
* OpenSubdiv now support multiple materials drawing in Cycles textured view.

<center>
{|
 |valign=top|[[File:object_info_node.png |300px|thumb|center|Object Info Node]]
 |valign=top|[[File:layer_weight_node.png |300px|thumb|center|Layer Weight and Fresnel nodes]]
 |}
</center>

== Freestyle Rendering ==

* The "Fill Range by Selection" tool in the "Distance from Camera/Object" modifier now only takes into account selected mesh vertices, for finer control. ([https://projects.blender.org/blender/blender/commit/4e1025376ea1 4e1025376e])
* The "Selection by Group" option in for Freestyle Line Sets now supports nested groups. ([https://projects.blender.org/blender/blender/commit/781507d2dded 781507d2dd])

== Particles ==

New tools to quickly duplicate a particle system on the same object, and an option to duplicate particle system settings along with the particle system. ([https://projects.blender.org/blender/blender/commit/1d03bc73cea8 1d03bc73ce], [https://projects.blender.org/blender/blender/commit/1925b9b2fac4 1925b9b2fa])

== Rigid Body Physics ==

Added support for rotational springs with stiffness and damping. ([https://projects.blender.org/blender/blender/commit/c8c7414c3f67 c8c7414c3f])

== Collada ==

* Added support for custom bind matrix, using new bind_mat custom property. ([https://projects.blender.org/blender/blender/commit/51d474303 51d474303])
* UV Textures and materials: both are now exported as materials for better compatibility with other application. Further, only one of the two is exported, avoiding wrong textures when multiple objects are exported.