= Blender 2.79a: Bug Fixes =

Changes from revision [https://projects.blender.org/blender/blender/commit/e8e40b171b  rBe8e40b17] to [https://projects.blender.org/blender/blender/commit/4c1bed0a12  rB4c1bed0a], inclusive.

Total fixed bugs: 202 (117 from tracker, 85 reported/found by other ways).

'''Note:''' Nearly all commits from 2.79 are listed here (over 75% of total), only really technical/non-user affecting ones have been skipped.

== Objects / Animation / GP ==

=== Animation ===
* Fix [http://developer.blender.org/T52932 #52932]: Driver with target of custom property from scene fails to update ([https://projects.blender.org/blender/blender/commit/ad834085b7  rBad834085]).
* Fix [http://developer.blender.org/T52835 #52835]: When driven IK influence change, ik animation have 1 frame delay ([https://projects.blender.org/blender/blender/commit/89de073ea5  rB89de073e]).

* Fix unreported: Fix missing ID remapping in Action editor callback ([https://projects.blender.org/blender/blender/commit/9f6d5d679e  rB9f6d5d67]).
* Fix unreported: [http://developer.blender.org/T50354 #50354]: Action length calculation added unnecessary padding if some F-Curves only contained a single key (on the last real frame of the action) ([https://projects.blender.org/blender/blender/commit/f887b8b230  rBf887b8b2]).
* Fix unreported: Fix: Undo pushes were missing for Add/Remove Driver Variable buttons, and Remove Driver button ([https://projects.blender.org/blender/blender/commit/08e16e0bd1  rB08e16e0b]).

=== Grease Pencil ===
* Fix unreported: Fix: When transforming GP strokes in "Local" mode, the strokes would get obscured by the transform constraint lines ([https://projects.blender.org/blender/blender/commit/99e4c819f7  rB99e4c819]).

=== Objects ===
* Fix [http://developer.blender.org/T52729 #52729]: Decimals not showing over 100m or 100 feet ([https://projects.blender.org/blender/blender/commit/5384b2a6e2  rB5384b2a6]).
* Fix [http://developer.blender.org/T52140 #52140]: Align objects centers using origin for text ([https://projects.blender.org/blender/blender/commit/3a0f199aa7  rB3a0f199a]).

=== Dependency Graph ===
* Fix [http://developer.blender.org/T52432 #52432]: Blender crashes while using Ghost (new depsgraph) ([https://projects.blender.org/blender/blender/commit/2213153c27  rB2213153c]).
* Fix [http://developer.blender.org/T52741 #52741]: Follow track with depth object crashes Blender with new depsgraph ([https://projects.blender.org/blender/blender/commit/d5dbe0c566  rBd5dbe0c5]).
* Fix [http://developer.blender.org/T53547 #53547]: Metaballs as dupli objects are not updated with the new Depsgraph ([https://projects.blender.org/blender/blender/commit/8cdda3d2ad  rB8cdda3d2]).

* Fix unreported: Transform: Enable recursion dependency check for new depsgraph ([https://projects.blender.org/blender/blender/commit/ef04aa4a43  rBef04aa4a]).
* Fix unreported: Depsgraph: Fix relations for metaballs ([https://projects.blender.org/blender/blender/commit/7103c6ef3b  rB7103c6ef]).

== Data / Geometry ==

=== Armatures ===
* Fix [http://developer.blender.org/T53300 #53300]: Bone Extrude via Ctrl + Click is not done from active bone tail ([https://projects.blender.org/blender/blender/commit/969196069a  rB96919606]).
* Fix [http://developer.blender.org/T53054 #53054]: Parentless bone + IK crashes ([https://projects.blender.org/blender/blender/commit/c63e08863d  rBc63e0886]).

=== Curve/Text Editing ===
* Fix [http://developer.blender.org/T52860 #52860]: 3D Text crashes w/ Ctrl Backspace ([https://projects.blender.org/blender/blender/commit/b969f7eb55  rBb969f7eb]).
* Fix [http://developer.blender.org/T53410 #53410]: 3D Text always recalculated ([https://projects.blender.org/blender/blender/commit/480b0b5dfc  rB480b0b5d]).
* Fix [http://developer.blender.org/T53586 #53586]: Surfaces collapse when joined ([https://projects.blender.org/blender/blender/commit/1611177ac9  rB1611177a]).

* Fix unreported: Curves: Fix wrong bitset being checked against CYCLIC bit flag ([https://projects.blender.org/blender/blender/commit/2edc2b4912  rB2edc2b49]).

=== Mesh Editing ===
* Fix [http://developer.blender.org/T53145 #53145]: bevel tool fails when used a second time ([https://projects.blender.org/blender/blender/commit/36f324fc55  rB36f324fc]).
* Fix [http://developer.blender.org/T53145 #53145]: bevel tool does not start with amount at zero ([https://projects.blender.org/blender/blender/commit/f1a4e130b3  rBf1a4e130]).
* Fix [http://developer.blender.org/T52723 #52723]: Reset UV layers failed ([https://projects.blender.org/blender/blender/commit/0641069778  rB06410697]).
* Fix [http://developer.blender.org/T52748 #52748]: Select shortest face path fails ([https://projects.blender.org/blender/blender/commit/60bfa969e2  rB60bfa969]).
* Fix [http://developer.blender.org/T52384 #52384]: Bridge pair result depends on other loops ([https://projects.blender.org/blender/blender/commit/495aa77b53  rB495aa77b]).
* Fix [http://developer.blender.org/T53131 #53131]: Incorrect vert-edge angle calculation ([https://projects.blender.org/blender/blender/commit/60e8d86fb1  rB60e8d86f]).
* Fix [http://developer.blender.org/T53441 #53441]: Inset doesn't start at zero ([https://projects.blender.org/blender/blender/commit/19b27d84ab  rB19b27d84]).
* Fix [http://developer.blender.org/T53529 #53529]: Rip crashes w/ wire edge ([https://projects.blender.org/blender/blender/commit/52a5daa404  rB52a5daa4]).
* Fix [http://developer.blender.org/T53343 #53343]: Custom Normal Data Transfer Crashes when some vertexes have no faces ([https://projects.blender.org/blender/blender/commit/ff8c9c5931  rBff8c9c59]).
* Fix [http://developer.blender.org/T53420 #53420]: Vertex Groups: The "-" button gets a hidden function ([https://projects.blender.org/blender/blender/commit/50ca70b275  rB50ca70b2]).
* Fix [http://developer.blender.org/T52733 #52733]: Percent  mode for Bevel sometimes had nans ([https://projects.blender.org/blender/blender/commit/48079e1f11  rB48079e1f]).
* Fix [http://developer.blender.org/T52871 #52871]: `BLI_polyfill_beautify_quad_rotate_calc_ex` was mistakenly considering the state as degenerated ([https://projects.blender.org/blender/blender/commit/9298d99b77  rB9298d99b]).
* Fix [http://developer.blender.org/T52871 #52871]: beauty fill error ([https://projects.blender.org/blender/blender/commit/9d501d23cb  rB9d501d23]).
* Fix [http://developer.blender.org/T53143 #53143]: Knife Crash after Grid Fill ([https://projects.blender.org/blender/blender/commit/4f247ed07a  rB4f247ed0]).
* Fix [http://developer.blender.org/T53713 #53713]: User remap failed w/ texface images ([https://projects.blender.org/blender/blender/commit/26ffade5c1  rB26ffade5]).
* Fix [http://developer.blender.org/T52818 #52818]: Tangent space calculation is really slow for high-density mesh with degenerated topology ([https://projects.blender.org/blender/blender/commit/9a5320aea3  rB9a5320ae]).
* Fix [http://developer.blender.org/T53678 #53678]: Smart Project UV margin ignores units ([https://projects.blender.org/blender/blender/commit/42e207b599  rB42e207b5]).

* Fix unreported: Fix for inset when accessed from spacebar search ([https://projects.blender.org/blender/blender/commit/db3e3f9c24  rBdb3e3f9c]).
* Fix unreported: Edit Mesh: click extrude, ensure inverse matrix ([https://projects.blender.org/blender/blender/commit/3e3a27d089  rB3e3a27d0]).
* Fix unreported: Polyfill Beautify: option to rotate out of degenerate state ([https://projects.blender.org/blender/blender/commit/0834eefae0  rB0834eefa]).
* Fix unreported: BMesh: use less involved check for edge rotation ([https://projects.blender.org/blender/blender/commit/58ab62ed6d  rB58ab62ed]).
* Fix unreported: Avoid bias when calculating quad split direction ([https://projects.blender.org/blender/blender/commit/569d2df634  rB569d2df6]).
* Fix unreported: Beauty fill was skipping small faces ([https://projects.blender.org/blender/blender/commit/1b6130533f  rB1b613053]).
* Fix unreported: Polyfill Beautify: half-edge optimization ([https://projects.blender.org/blender/blender/commit/4a457d4f1e  rB4a457d4f]).
* Fix unreported: Use BLI_heap_reinsert for decimate and beautify ([https://projects.blender.org/blender/blender/commit/eaeb0a002e  rBeaeb0a00]).
* Fix unreported: BMesh: move bridge tools stepping logic into macro ([https://projects.blender.org/blender/blender/commit/02b206780e  rB02b20678]).
* Fix unreported: Subsurf: Avoid global lock for loops and orig index layers ([https://projects.blender.org/blender/blender/commit/83b0603061  rB83b06030]).
* Fix unreported: Subsurf: Avoid possible use of partially initialized edge hash ([https://projects.blender.org/blender/blender/commit/72151f3e36  rB72151f3e]).
* Fix unreported: Fix scalability issue in threaded code of Mesh normals computation ([https://projects.blender.org/blender/blender/commit/71e0894e0d  rB71e0894e]).

=== Modifiers ===
* Fix [http://developer.blender.org/T51074 #51074]: Boolean modifier inverts operation ([https://projects.blender.org/blender/blender/commit/d07011da71  rBd07011da]).
* Fix [http://developer.blender.org/T52291 #52291]: Boolean fails w/ co-linear edged ngons ([https://projects.blender.org/blender/blender/commit/78cc3c828f  rB78cc3c82]).
* Fix [http://developer.blender.org/T52763 #52763]: Boolean problem with vertex group ([https://projects.blender.org/blender/blender/commit/00d8097510  rB00d80975]).
* Fix [http://developer.blender.org/T52823 #52823]: New Depsgraph - Shrinkwrap crashes blender ([https://projects.blender.org/blender/blender/commit/754630cee4  rB754630ce]).
* Fix [http://developer.blender.org/T53007 #53007]: OpenSubdiv + transparency = artefact/crashes ([https://projects.blender.org/blender/blender/commit/ba40d8f331  rBba40d8f3]).
* Fix [http://developer.blender.org/T53398 #53398]: Surface deform modifier says that convex polygons are concave for big faces ([https://projects.blender.org/blender/blender/commit/4c46f69376  rB4c46f693]).

* Fix unreported: Fix (irc-reported by @sergey) invalid precision value in a float RNA property ([https://projects.blender.org/blender/blender/commit/8e43a9e9cc  rB8e43a9e9]).

=== Material / Texture ===
* Fix [http://developer.blender.org/T53116 #53116]: default texture coordinates for volume materials are blank ([https://projects.blender.org/blender/blender/commit/576899b90a  rB576899b9]).
* Fix [http://developer.blender.org/T52953 #52953]: Crash removing material ([https://projects.blender.org/blender/blender/commit/243b961c29  rB243b961c]).
* Fix [http://developer.blender.org/T53509 #53509]: Datablock ID Properties attached to bpy.types.Material are not loaded ([https://projects.blender.org/blender/blender/commit/0c365472b6  rB0c365472]).

* Fix unreported: Fix logic for pinning textures users from context ([https://projects.blender.org/blender/blender/commit/8f9d9ba14e  rB8f9d9ba1]).

== Physics / Simulations / Sculpt / Paint ==

=== Particles ===
* Fix [http://developer.blender.org/T53552 #53552]: Unneeded particle cache reset on frame change ([https://projects.blender.org/blender/blender/commit/6f19787e52  rB6f19787e]).
* Fix [http://developer.blender.org/T52732 #52732]: Particle system volume grid particles out of volume ([https://projects.blender.org/blender/blender/commit/17c00d222f  rB17c00d22]).
* Fix [http://developer.blender.org/T53513 #53513]: Particle size showing in multiple places ([https://projects.blender.org/blender/blender/commit/c70a45027d  rBc70a4502]).
* Fix [http://developer.blender.org/T53832 #53832]: Particle weight paint crash ([https://projects.blender.org/blender/blender/commit/5b3538e02a  rB5b3538e0]).
* Fix [http://developer.blender.org/T53823 #53823]: Particle weight brush crash ([https://projects.blender.org/blender/blender/commit/b6481cbbe5  rBb6481cbb]).

* Fix unreported: Fix missing update for particles w/ fluids ([https://projects.blender.org/blender/blender/commit/84361a0709  rB84361a07]).

=== Physics / Hair / Simulations ===
* Fix [http://developer.blender.org/T53650 #53650]: remove hard limits on force field size and max distance ([https://projects.blender.org/blender/blender/commit/2a9abc0f5e  rB2a9abc0f]).

* Fix unreported: Fix error copying smoke modifier uv layer ([https://projects.blender.org/blender/blender/commit/9f032c3867  rB9f032c38]).
* Fix unreported: Depsgraph: Don't make non-dynamic hair dependent on time ([https://projects.blender.org/blender/blender/commit/76032b133c  rB76032b13]).

=== Sculpting / Painting ===
* Fix [http://developer.blender.org/T53577 #53577]: Rake sculpt/paint wrong on first step ([https://projects.blender.org/blender/blender/commit/413817a3d2  rB413817a3]).
* Fix [http://developer.blender.org/T52537 #52537]: Dyntopo "detail flood fill" doesn't work in some cases ([https://projects.blender.org/blender/blender/commit/1b8e8326b4  rB1b8e8326]).
* Fix [http://developer.blender.org/T53593 #53593]: sculpt brush rake spacing bug after recent bugfix ([https://projects.blender.org/blender/blender/commit/4c1bed0a12  rB4c1bed0a]).

* Fix unreported: Fix brush reset (missing notifier) ([https://projects.blender.org/blender/blender/commit/98dc9072e5  rB98dc9072]).
* Fix unreported: Fix sculpt secondary color missing some brushes ([https://projects.blender.org/blender/blender/commit/6058b651c4  rB6058b651]).

== Image / Video / Render ==

=== Image / UV Editing ===
* Fix [http://developer.blender.org/T53092 #53092]: errors reading EXR files with different data/display window ([https://projects.blender.org/blender/blender/commit/f901bf6f47  rBf901bf6f]).
* Fix [http://developer.blender.org/T52739 #52739]: Crash loading corrupted video files ([https://projects.blender.org/blender/blender/commit/9c39f021ad  rB9c39f021]).
* Fix [http://developer.blender.org/T52811 #52811]: At any framerate selected, video exported with 1000fps ([https://projects.blender.org/blender/blender/commit/3163d0e205  rB3163d0e2]).
* Fix [http://developer.blender.org/T52920 #52920]: Saving Tiff Files type Blender crashes ([https://projects.blender.org/blender/blender/commit/d305c10104  rBd305c101]).
* Fix [http://developer.blender.org/T53499 #53499]: Cannot load DPX files ([https://projects.blender.org/blender/blender/commit/b8bdca8c0a  rBb8bdca8c]).

* Fix unreported: Fix writing Iris images w/ invalid header ([https://projects.blender.org/blender/blender/commit/acae901a10  rBacae901a]).

=== Masking ===
* Fix [http://developer.blender.org/T52840 #52840]: New Depsgraph - Mask editor not working correctly ([https://projects.blender.org/blender/blender/commit/69062cdd8d  rB69062cdd]).
* Fix [http://developer.blender.org/T52749 #52749]: New Depsgraph - Render View Mask is not initialized correctly ([https://projects.blender.org/blender/blender/commit/538182511a  rB53818251]).
* Fix [http://developer.blender.org/T53419 #53419]: Masking "Add" menu is not present in Image editor, but shortcut is ([https://projects.blender.org/blender/blender/commit/c8f95c7829  rBc8f95c78]).

=== Motion Tracking ===
* Fix [http://developer.blender.org/T52851 #52851]: Per-frame traking is broken when sequence doesn't start at frame 1 ([https://projects.blender.org/blender/blender/commit/f0743b56ec  rBf0743b56]).
* Fix [http://developer.blender.org/T53612 #53612]: Blender crashes on CleanTracks with 'DELETE_SEGMENTS' and a disabled marker ([https://projects.blender.org/blender/blender/commit/a1d05ac2a1  rBa1d05ac2]).

* Fix unreported: Tracking: Fix crash when tracking failed ([https://projects.blender.org/blender/blender/commit/350f5c6894  rB350f5c68]).
* Fix unreported: Tracking: Create mesh from selected tracks only ([https://projects.blender.org/blender/blender/commit/333ef6e6f7  rB333ef6e6]).

=== Nodes / Compositor ===
* Fix [http://developer.blender.org/T53360 #53360]: crash with GLSL bump mapping and missing group output node ([https://projects.blender.org/blender/blender/commit/bb89759624  rBbb897596]).
* Fix [http://developer.blender.org/T52299 #52299]: X resolution of 4 causes nodes to collapse ([https://projects.blender.org/blender/blender/commit/cdc35e63bd  rBcdc35e63]).
* Fix [http://developer.blender.org/T53371 #53371]: Keying Node fails with values above 1 ([https://projects.blender.org/blender/blender/commit/1c7657befb  rB1c7657be]).
* Fix [http://developer.blender.org/T52113 #52113]: Compositor doesnt mix unrendered render layers well ([https://projects.blender.org/blender/blender/commit/3182336666  rB31823366]).
* Fix [http://developer.blender.org/T53263 #53263]: Proposed Blender crashes when rendering with Stabilizer 2D node without movie selected ([https://projects.blender.org/blender/blender/commit/ce35151f38  rBce35151f]).
* Fix [http://developer.blender.org/T53696 #53696]: Compositor HSV limits changed ([https://projects.blender.org/blender/blender/commit/528e00dae0  rB528e00da]).
* Fix [http://developer.blender.org/T52927 #52927]: Compositor wrong scale when scale size input is connected to complex node ([https://projects.blender.org/blender/blender/commit/259e9ad00d  rB259e9ad0]).
* Fix [http://developer.blender.org/T47212 #47212]: Luminance Key not working with HDR and out-of-gamut ranges ([https://projects.blender.org/blender/blender/commit/13973a5bbf  rB13973a5b]).

* Fix unreported: Fix compositor node links getting lost on file load for custom render passes ([https://projects.blender.org/blender/blender/commit/6bd1189e5c  rB6bd1189e]).
* Fix unreported: Node selection: Stop operator when mouse selection selected a node ([https://projects.blender.org/blender/blender/commit/bf58ec9265  rBbf58ec92]).
* Fix unreported: Compositor: Ensured 16 byte alignment for variables accessed by SSE instructions ([https://projects.blender.org/blender/blender/commit/02bac54fa9  rB02bac54f]).
* Fix unreported: (Nodes) Display image name if any in the Image and Texture Image node title ([https://projects.blender.org/blender/blender/commit/010cf35e7e  rB010cf35e]).
* Fix unreported: Fix old files with changed node socket type not loading correctly ([https://projects.blender.org/blender/blender/commit/79563d2a33  rB79563d2a]).

=== Render ===
* Fix [http://developer.blender.org/T52679 #52679]: Hole in bake normal ([https://projects.blender.org/blender/blender/commit/759af7f1ee  rB759af7f1]).
* Fix [http://developer.blender.org/T53185 #53185]: After rendering an animation (Ctrl-F12), pressing F12 no longer renders single frames only ([https://projects.blender.org/blender/blender/commit/6cfc5edb86  rB6cfc5edb]).
* Fix [http://developer.blender.org/T53810 #53810]: Crash removing a scene used in render ([https://projects.blender.org/blender/blender/commit/5ddcad4377  rB5ddcad43]).

* Fix unreported: Fix incorrect color management when saving JPG previews for EXR ([https://projects.blender.org/blender/blender/commit/1dbdcfe9a9  rB1dbdcfe9]).
* Fix unreported: Fix leak when rendering OpenGL animations ([https://projects.blender.org/blender/blender/commit/e4dce3b3d7  rBe4dce3b3]).

=== Render: Cycles ===
* Fix [http://developer.blender.org/T53129 #53129]: Cycles missing update when changing image auto refresh ([https://projects.blender.org/blender/blender/commit/a6e5558194  rBa6e55581]).
* Fix [http://developer.blender.org/T53217 #53217]: GLSL principled BSDF black with zero clearcoat roughness ([https://projects.blender.org/blender/blender/commit/d564beda44  rBd564beda]).
* Fix [http://developer.blender.org/T52368 #52368]: Cycles OSL trace() failing on Windows 32 bit ([https://projects.blender.org/blender/blender/commit/2bc667ec79  rB2bc667ec]).
* Fix [http://developer.blender.org/T53348 #53348]: Cycles difference between gradient texture on CPU and GPU ([https://projects.blender.org/blender/blender/commit/198fd0be43  rB198fd0be]).
* Fix [http://developer.blender.org/T53171 #53171]: lamp specials strength tweak fails with renamed emission nodes ([https://projects.blender.org/blender/blender/commit/5b2d5f9077  rB5b2d5f90]).
* Fix [http://developer.blender.org/T52573 #52573]: Cycles baking artifacts ([https://projects.blender.org/blender/blender/commit/b0c55d5c94  rBb0c55d5c]).
* Fix [http://developer.blender.org/T51416 #51416]: Blender Crashes while moving Sliders ([https://projects.blender.org/blender/blender/commit/aafe528c7d  rBaafe528c]).
* Fix [http://developer.blender.org/T53048 #53048]: OSL Volume is broken in Blender 2.79 ([https://projects.blender.org/blender/blender/commit/ad7385422e  rBad738542]).
* Fix [http://developer.blender.org/T53559 #53559]: Auto texture space for text and font is wrong in Cycles ([https://projects.blender.org/blender/blender/commit/e7aad8fd0e  rBe7aad8fd]).
* Fix [http://developer.blender.org/T53600 #53600]: Cycles shader mixing issue with principled BSDF and zero weights ([https://projects.blender.org/blender/blender/commit/836a1ccf72  rB836a1ccf]).
* Fix [http://developer.blender.org/T52801 #52801]: reload scripts causes Cycles viewport render crash ([https://projects.blender.org/blender/blender/commit/f1ee24a284  rBf1ee24a2]).
* Fix [http://developer.blender.org/T53017 #53017]: Cycles not detecting AMD GPU when there is an NVidia GPU too ([https://projects.blender.org/blender/blender/commit/1f50f0676a  rB1f50f067]).
* Fix [http://developer.blender.org/T53755 #53755]: Cycles OpenCL lamp shaders have incorrect normal ([https://projects.blender.org/blender/blender/commit/2ca933f457  rB2ca933f4]).
* Fix [http://developer.blender.org/T53692 #53692]: OpenCL multi GPU rendering not using all GPUs ([https://projects.blender.org/blender/blender/commit/30a0459f2c  rB30a0459f]).
* Fix [http://developer.blender.org/T53567 #53567]: Negative pixel values causing artifacts with denoising ([https://projects.blender.org/blender/blender/commit/824c039230  rB824c0392]).
* Fix [http://developer.blender.org/T53012 #53012]: Shadow catcher creates artifacts on contact area ([https://projects.blender.org/blender/blender/commit/c3237cdc13  rBc3237cdc]).

* Fix unreported: OpenVDB: Fix compilation error against OpenVDB 4 ([https://projects.blender.org/blender/blender/commit/0cdfe887b4  rB0cdfe887]).
* Fix unreported: Cycles: Fix possible race condition when generating Beckmann table ([https://projects.blender.org/blender/blender/commit/b5c629e604  rBb5c629e6]).
* Fix unreported: Fix Cycles bug in RR termination, probability should never be > 1.0 ([https://projects.blender.org/blender/blender/commit/df1af9b349  rBdf1af9b3]).
* Fix unreported: CMake: support CUDA 9 toolkit, and automatically disable sm_2x binaries ([https://projects.blender.org/blender/blender/commit/cd9c68f0c5  rBcd9c68f0]).
* Fix unreported: Fix part of [http://developer.blender.org/T53038 #53038]: principled BSDF clearcoat weight has no effect with 0 roughness ([https://projects.blender.org/blender/blender/commit/440d647cc1  rB440d647c]).
* Fix unreported: Fix incorrect MIS with principled BSDF and specular roughness 0 ([https://projects.blender.org/blender/blender/commit/866be3423c  rB866be342]).
* Fix unreported: Cycles: Fix possible race condition when initializing devices list ([https://projects.blender.org/blender/blender/commit/93d711ce55  rB93d711ce]).
* Fix unreported: Cycles: Fix wrong shading when some mesh triangle has non-finite coordinate ([https://projects.blender.org/blender/blender/commit/075950ad66  rB075950ad]).
* Fix unreported: Cycles: Fix compilation error with latest OIIO ([https://projects.blender.org/blender/blender/commit/49f57e5346  rB49f57e53]).
* Fix unreported: Cycles: Fix compilation error with OIIO compiled against system PugiXML ([https://projects.blender.org/blender/blender/commit/b6f3fec259  rBb6f3fec2]).
* Fix unreported: Cycles: Fix compilation error of standalone application ([https://projects.blender.org/blender/blender/commit/391f7cc406  rB391f7cc4]).
* Fix unreported: Cycles: Workaround for performance loss with the CUDA 9.0 SDK ([https://projects.blender.org/blender/blender/commit/b3adce7766  rBb3adce77]).
* Fix unreported: Cycles: Fix difference in image Clip extension method between CPU and GPU ([https://projects.blender.org/blender/blender/commit/a50c381fac  rBa50c381f]).
* Fix unreported: Cycles: Fix crash opening user preferences after adding extra GPU ([https://projects.blender.org/blender/blender/commit/a3616980c6  rBa3616980]).
* Fix unreported: Cycles: Fix bug in user preferences with factory startup ([https://projects.blender.org/blender/blender/commit/2f5a027b94  rB2f5a027b]).

=== Sequencer ===
* Fix [http://developer.blender.org/T52890 #52890]: Crash unlinking sequencer sound ([https://projects.blender.org/blender/blender/commit/eaba111bd5  rBeaba111b]).
* Fix [http://developer.blender.org/T53430 #53430]: Cut at the strip end fails w/ endstill ([https://projects.blender.org/blender/blender/commit/5ab1897de7  rB5ab1897d]).
* Fix [http://developer.blender.org/T52940 #52940]: VSE Glow Effect Strip on transparent images has no blur ([https://projects.blender.org/blender/blender/commit/3ad84309df  rB3ad84309]).
* Fix [http://developer.blender.org/T53639 #53639]: text sequence strips no stamped into render ([https://projects.blender.org/blender/blender/commit/8f7030e5f3  rB8f7030e5]).

== UI / Spaces / Transform ==

=== 3D View ===
* Fix [http://developer.blender.org/T52959 #52959]: Local view looses clip range on exit ([https://projects.blender.org/blender/blender/commit/7f6e9c595b  rB7f6e9c59]).
* Fix [http://developer.blender.org/T53850 #53850]: Lock to Cursor breaks 3D manipulators ([https://projects.blender.org/blender/blender/commit/b90f3928a3  rBb90f3928]).

* Fix unreported: Fix ruler access from search pop-up ([https://projects.blender.org/blender/blender/commit/b0fbe95a1b  rBb0fbe95a]).
* Fix unreported: 3D View: use shortest angle between quaternions ([https://projects.blender.org/blender/blender/commit/2895cb22a7  rB2895cb22]).

=== Input (NDOF / 3D Mouse) ===
* Fix [http://developer.blender.org/T52998 #52998]: disabled menu entries responding to key shortcuts ([https://projects.blender.org/blender/blender/commit/20c96cce86  rB20c96cce]).
* Fix [http://developer.blender.org/T52861 #52861]: Keymap editor filter doesn't show shortcuts using "+" ([https://projects.blender.org/blender/blender/commit/ca236408f3  rBca236408]).

=== Outliner ===
* Fix [http://developer.blender.org/T53342 #53342]: Outliner 'select hierarchy' broken ([https://projects.blender.org/blender/blender/commit/7153cee305  rB7153cee3]).

=== Transform ===
* Fix [http://developer.blender.org/T53463 #53463]: Rotation numerical input shows instable behaviour ([https://projects.blender.org/blender/blender/commit/8a9d64b578  rB8a9d64b5]).
* Fix [http://developer.blender.org/T53309 #53309]: Remove default 'Clear loc/rot/scale delta transform' shortcuts ([https://projects.blender.org/blender/blender/commit/c4f8d924e1  rBc4f8d924]).
* Fix [http://developer.blender.org/T52086 #52086]: Graph editor "normalize" drag errors for integers ([https://projects.blender.org/blender/blender/commit/9f916baa70  rB9f916baa]).
* Fix [http://developer.blender.org/T53311 #53311]: transform edge/normal orientation ([https://projects.blender.org/blender/blender/commit/57b11d8b4e  rB57b11d8b]).

=== User Interface ===
* Fix [http://developer.blender.org/T52800 #52800]: fix UI flickering with Mesa on Linux ([https://projects.blender.org/blender/blender/commit/c85a305c09  rBc85a305c]).
* Fix [http://developer.blender.org/T53630 #53630]: Effect strips not disFix [http://developer.blender.org/T52977 #52977]: playing Input data. Parent bone name disappeared in the UI in pose mode ([https://projects.blender.org/blender/blender/commit/4bc89d815c  rB4bc89d81]).

* Fix unreported: Fix failure in our UI code that could allow search button without search callbacks, leading to crash ([https://projects.blender.org/blender/blender/commit/e8e40b171b  rBe8e40b17]).
* Fix unreported: Add some security checks against future bad float UIprecision values ([https://projects.blender.org/blender/blender/commit/7e4be7d348  rB7e4be7d3]).
* Fix unreported: UI: avoid int cast before clamping number input ([https://projects.blender.org/blender/blender/commit/a437b9bbbe  rBa437b9bb]).
* Fix unreported: UI: fullstop at end of tooltips ([https://projects.blender.org/blender/blender/commit/4c3e2518c8  rB4c3e2518]).
* Fix unreported: Fix incorrect allocation size ([https://projects.blender.org/blender/blender/commit/6b2d1f63db  rB6b2d1f63]).

== Game Engine ==
* Fix unreported: Fix BGE sound actuator property access ([https://projects.blender.org/blender/blender/commit/d2d207773e  rBd2d20777]).

== System / Misc ==

=== Collada ===
* Fix [http://developer.blender.org/T53322 #53322]: Collada export crash w/ shape keys ([https://projects.blender.org/blender/blender/commit/869e4a3420  rB869e4a34]).
* Fix [http://developer.blender.org/T53230 #53230]: avoid Nullpointer problems in Collada Exporter ([https://projects.blender.org/blender/blender/commit/eaf14b1ebf  rBeaf14b1e]).
* Fix [http://developer.blender.org/T52831 #52831]: removed enforcement of matrix decomposition when animations are exported ([https://projects.blender.org/blender/blender/commit/a123dafdc0  rBa123dafd]).

=== File I/O ===
* Fix [http://developer.blender.org/T52816 #52816]: regression can't open file in 2.79 (crash) ([https://projects.blender.org/blender/blender/commit/a4116673c7  rBa4116673]).
* Fix [http://developer.blender.org/T53002 #53002]: Batch-Generate Previews generate empty or none image for large objects ([https://projects.blender.org/blender/blender/commit/47a388b2a9  rB47a388b2]).
* Fix [http://developer.blender.org/T52514 #52514]: don't clear filename when dropping directory path in file browser ([https://projects.blender.org/blender/blender/commit/625b2f5dab  rB625b2f5d]).
* Fix [http://developer.blender.org/T53250 #53250]: Crash when linking/appending a scene to a blend when another linked scene in this blend is currently open/active ([https://projects.blender.org/blender/blender/commit/06df30a415  rB06df30a4]).
* Fix [http://developer.blender.org/T53572 #53572]: Alembic imports UV maps incorrectly ([https://projects.blender.org/blender/blender/commit/0f841e24b0  rB0f841e24]).

* Fix unreported: Alembic import: fixed mesh corruption when changing topology ([https://projects.blender.org/blender/blender/commit/bae796f0d5  rBbae796f0]).

=== Other ===
* Fix [http://developer.blender.org/T53274 #53274]: Saving template prefs overwrites default prefs ([https://projects.blender.org/blender/blender/commit/38fdfe757d  rB38fdfe75]).
* Fix [http://developer.blender.org/T53637 #53637]: Keymap from app-template ignored ([https://projects.blender.org/blender/blender/commit/8c484d0bda  rB8c484d0b]).

* Fix unreported: Fix potential string buffer overruns ([https://projects.blender.org/blender/blender/commit/30f53d56b6  rB30f53d56]).
* Fix unreported: Fix build with OSL 1.9.x, automatically aligns to 16 bytes now ([https://projects.blender.org/blender/blender/commit/f968268c1e  rBf968268c]).
* Fix unreported: WM: minor correction to user-pref writing ([https://projects.blender.org/blender/blender/commit/75aec5eeaa  rB75aec5ee]).
* Fix unreported: WM: don't load preferences on 'File -> New' ([https://projects.blender.org/blender/blender/commit/571e801b27  rB571e801b]).
* Fix unreported: WM: load UI for new file, even when pref disabled ([https://projects.blender.org/blender/blender/commit/9f0ebb0941  rB9f0ebb09]).
* Fix unreported: Fix MSVSC2017 error ([https://projects.blender.org/blender/blender/commit/405874bd79  rB405874bd]).
* Fix unreported: Fix SGI foramt reader CVE-2017-2901 ([https://projects.blender.org/blender/blender/commit/7b8b621c1d  rB7b8b621c]).
* Fix unreported: Memory: add MEM_malloc_arrayN() function to protect against overflow ([https://projects.blender.org/blender/blender/commit/a972729895  rBa9727298]).
* Fix unreported: Fix buffer overflows in TIFF, PNG, IRIS, DPX, HDR and AVI loading ([https://projects.blender.org/blender/blender/commit/16718fe4ea  rB16718fe4]).
* Fix unreported: Fix buffer overflow vulernability in thumbnail file reading ([https://projects.blender.org/blender/blender/commit/04c5131281  rB04c51312]).
* Fix unreported: Fix buffer overflow vulnerabilities in mesh code ([https://projects.blender.org/blender/blender/commit/9287434fa1  rB9287434f]).
* Fix unreported: Fix buffer overflow vulnerability in curve, font, particles code ([https://projects.blender.org/blender/blender/commit/8dbd5ea4c8  rB8dbd5ea4]).
* Fix unreported: Fix manual lookups (data is now lowercase) ([https://projects.blender.org/blender/blender/commit/cae8c68ca6  rBcae8c68c]).

=== Python ===
* Fix [http://developer.blender.org/T53273 #53273]: render bake settings properties not showing correct Python path ([https://projects.blender.org/blender/blender/commit/825aecaee3  rB825aecae]).
* Fix [http://developer.blender.org/T52442 #52442]: bl_app_templates_system not working ([https://projects.blender.org/blender/blender/commit/d89353159f  rBd8935315]).
* Fix [http://developer.blender.org/T53294 #53294]: bpy.ops.image.open crash ([https://projects.blender.org/blender/blender/commit/8ea3f6438d  rB8ea3f643]).
* Fix [http://developer.blender.org/T53191 #53191]: Python API Reference link wrong in splash screen ([https://projects.blender.org/blender/blender/commit/5f2307d1a7  rB5f2307d1]).
* Fix [http://developer.blender.org/T52982 #52982]: Join operator with context override crashes Blender 2.79 ([https://projects.blender.org/blender/blender/commit/09c387269a  rB09c38726]).
* Fix [http://developer.blender.org/T53772 #53772]: Presets don't support colons ([https://projects.blender.org/blender/blender/commit/91ce295796  rB91ce2957]).

* Fix unreported: Fix bpy.utils.resource_path('SYSTEM') output ([https://projects.blender.org/blender/blender/commit/21ecdacdf1  rB21ecdacd]).
* Fix unreported: Fix setting the operator name in Py operator API ([https://projects.blender.org/blender/blender/commit/8c9bebfe28  rB8c9bebfe]).
* Fix unreported: Docs: add note for bmesh face_split_edgenet ([https://projects.blender.org/blender/blender/commit/a253b1b7bc  rBa253b1b7]).
* Fix unreported: Fix edge-split bmesh operator giving empty result ([https://projects.blender.org/blender/blender/commit/ce6e30b3dc  rBce6e30b3]).
* Fix unreported: Fix BMesh PyAPI internal flag clearing logic ([https://projects.blender.org/blender/blender/commit/6639350f92  rB6639350f]).
* Fix unreported: Docs: clarify return value for BVH API ([https://projects.blender.org/blender/blender/commit/0c456eb90a  rB0c456eb9]).
* Fix unreported: bl_app_override: support empty UI layout items ([https://projects.blender.org/blender/blender/commit/6d640504e8  rB6d640504]).
* Fix unreported: Fix bad 'poll' prop callback API doc ([https://projects.blender.org/blender/blender/commit/3193045c59  rB3193045c]).
* Fix unreported: Fix background_job template ([https://projects.blender.org/blender/blender/commit/38357cd004  rB38357cd0]).
* Fix unreported: Fix bmesh.utils.face_join arg parsing ([https://projects.blender.org/blender/blender/commit/20cccb1561  rB20cccb15]).

=== System ===
* Fix [http://developer.blender.org/T53004 #53004]: XWayland ignores cursor-warp calls ([https://projects.blender.org/blender/blender/commit/1ab828d4bb  rB1ab828d4]).
* Fix [http://developer.blender.org/T53068 #53068]: AMD Threadripper not working well with Blender ([https://projects.blender.org/blender/blender/commit/dfed7c48ac  rBdfed7c48]).

* Fix unreported: GPU: Fix memory corruption in GPU_debug on GTX1080 ([https://projects.blender.org/blender/blender/commit/3a1ade22e5  rB3a1ade22]).
* Fix unreported: Task scheduler: Start with suspended pool to avoid threading overhead on push ([https://projects.blender.org/blender/blender/commit/8553449cf2  rB8553449c]).


<hr/>

