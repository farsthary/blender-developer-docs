= Blender 2.92: Modeling =

== Tools ==

* Primitive Add: new interactive tool to create primitives with two clicks. ([https://projects.blender.org/blender/blender/commit/122cb1a 122cb1a])

[[File:Add object with objects in background.png|700px|thumb|center|Add object tool]]

[[File:Add object.mp4|700px|thumb|center|Add object tool]]

== Modifiers == 
* Operator to copy a single modifier to all selected objects ([https://projects.blender.org/blender/blender/commit/6fbeb6e2e05408af448e 6fbeb6e2e0])
* The bevel modifier now uses the angle limit method by default ([https://projects.blender.org/blender/blender/commit/6b5e4ad5899d87a183c4a3 6b5e4ad589])