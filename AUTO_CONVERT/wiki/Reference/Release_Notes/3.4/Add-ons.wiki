= Add-ons = 

==Image import as Plane==
* Update improving the UI and adding several common material settings ([http://developer.blender.org/D15744 D15744], [https://projects.blender.org/blender/blender-addons/commit/2ec6aba72cd3  rBA2ec6aba]).

== New add-ons ==
* New Storypencil add-on for Storyboarding in addons-contrib. ([http://developer.blender.org/T100665 T100665])

== glTF 2.0 ==

=== Importer ===

* Fix import SK when there is no target on each primitives ([https://projects.blender.org/blender/blender-addons/commit/70ee1a5b6600  rBA70ee1a5])
* Code cleanup ([https://projects.blender.org/blender/blender-addons/commit/b9c0e2878e91  rBAb9c0e28])
* Add hook before glTF import ([https://projects.blender.org/blender/blender-addons/commit/a1706bd0f04b  rBAa1706bd])
* Tweaks vertex color import ([https://projects.blender.org/blender/blender-addons/commit/cd2d9df92577  rBAcd2d9df])
* Fix ortho camera frame import/export ([https://projects.blender.org/blender/blender-addons/commit/8a2443844daf  rBA8a24438])
* '''Import/Export lights using correct units''' ([https://projects.blender.org/blender/blender-addons/commit/9d903a93f03b  rBA9d903a9])

=== Exporter ===

* '''Reset pose bone between each action''' ([https://projects.blender.org/blender/blender-addons/commit/e77b55e45a2a  rBAe77b55e])
* '''Option to export Active Collection only / with or without nested''' ([https://projects.blender.org/blender/blender-addons/commit/90732dddff7e  rBA90732dd], [https://projects.blender.org/blender/blender-addons/commit/712f007c7179  rBA712f007])
* '''Big refactoring of primitive extraction, manage custom attributes''' ([https://projects.blender.org/blender/blender-addons/commit/51e15a9db4ce  rBA51e15a9], [https://projects.blender.org/blender/blender-addons/commit/33e87824dc33  rBA33e8782], [https://projects.blender.org/blender/blender-addons/commit/7e2fa377ab1f  rBA7e2fa37], [https://projects.blender.org/blender/blender-addons/commit/4be7119ac5a7  rBA4be7119])
* Fix object parented to bone when using rest pose ([https://projects.blender.org/blender/blender-addons/commit/4cb6ebd8747e  rBA4cb6ebd], [https://projects.blender.org/blender/blender-addons/commit/106cb51ab9ad  rBA106cb51], [https://projects.blender.org/blender/blender-addons/commit/eb07144ea7e8  rBAeb07144])
* Manage delta transforms animations ([https://projects.blender.org/blender/blender-addons/commit/58db76bcc6ae  rBA58db76b], [https://projects.blender.org/blender/blender-addons/commit/726d08c9036b  rBA726d08c])
* Avoid crash when using apply modifiers + shapekeys ([https://projects.blender.org/blender/blender-addons/commit/bb77e697e13e  rBAbb77e69])
* Fix scene evaluation ([https://projects.blender.org/blender/blender-addons/commit/3c19be6ffa06  rBA3c19be6])
* Fix resolve UVMap ([https://projects.blender.org/blender/blender-addons/commit/bccd6c669db6  rBAbccd6c6])
* Better skin checks ([https://projects.blender.org/blender/blender-addons/commit/97bb515d3ac4  rBA97bb515])
* Clamp base color factor to [0,1] ([https://projects.blender.org/blender/blender-addons/commit/e890169e0a62  rBAe890169])
* Various hooks & fixes for animation ([https://projects.blender.org/blender/blender-addons/commit/eb3dcfc70c7f  rBAeb3dcfc])
* Avoid adding multiple neutral bone on same armature ([https://projects.blender.org/blender/blender-addons/commit/846679918718  rBA8466799])
* Fix TRS when parent is skined ([https://projects.blender.org/blender/blender-addons/commit/83290c67a63b  rBA83290c6])
* Fix ortho camera frame import/export ([https://projects.blender.org/blender/blender-addons/commit/8a2443844daf  rBA8a24438])
* '''Import/Export lights using correct units''' ([https://projects.blender.org/blender/blender-addons/commit/9d903a93f03b  rBA9d903a9])
* Non active action objects TRS is now restored at end of actions ([https://projects.blender.org/blender/blender-addons/commit/5dbcd4a3889f  rBA5dbcd4a])
* When user don't export SK, driver corrective SK animations must not be exported ([https://projects.blender.org/blender/blender-addons/commit/c4dddb88e7a7  rBAc4dddb8])
* Do not export special attributes, used internally by Blender ([https://projects.blender.org/blender/blender-addons/commit/addeb64f82f4  rBAaddeb64])
* Export SK on no modifiers objects even if 'apply modifier' is on ([https://projects.blender.org/blender/blender-addons/commit/896ee4698f25  rBA896ee46])
* Fix Traceback in Variant UI ([https://projects.blender.org/blender/blender-addons/commit/61ff4faf8c7d  rBA61ff4fa])
* Fix exporting skined mesh without armature ([https://projects.blender.org/blender/blender-addons/commit/0b4844ede970  rBA0b4844e])
* Fix crash with weird missing datablock node group ([https://projects.blender.org/blender/blender-addons/commit/b665525d03ce  rBAb665525])