= Blender 2.90: Modeling =

== Tools ==

* Extrude Manifold: new tool to automatically split and remove adjacent faces when extruding inwards. ([https://projects.blender.org/blender/blender/commit/b79a5bdd5ae0 b79a5bdd5a]).
* Ruler: snapping support for ruler tool. ([https://projects.blender.org/blender/blender/commit/1f7a791 1f7a791])
* Topology cleanup oriented tools Merge (including By Distance aka Remove Doubles), Rip, Delete, Dissolve, Connect Path, Knife and Tris To Quads now correctly preserve custom normal vectors. ([https://projects.blender.org/blender/blender/commit/93c8955a7 93c8955a7])
* The custom profile for the bevel modifier and tool now supports bezier curve handle types. ([https://projects.blender.org/blender/blender/commit/baff05ad1c15 baff05ad1c])
[[File:Curve Profile Handles 2.90.png|600px|thumb|center|Free, aligned, vector, and automatic handle types]]
* The bevel tool and modifier have a new 'Absolute' mode for interpreting the 'amount' value. This mode is like Percent, but measures absolute distance along adjacent edges instead of a percentage. [https://projects.blender.org/blender/blender/commit/466e716495ff 466e716495])
* The bevel tool and modifier use a new method to decide which material and UV values to use for the center polygons in odd-segment bevels. It should give more consistency in different parts of the model compared to the seemingly random choice made up til now. [https://projects.blender.org/blender/blender/commit/e3a420c4773a e3a420c477]

== UV Editor ==
* The new mesh editing options '''Correct Face Attributes''' and '''Keep Connected''' adjust UV's and vertex colors when moving mesh components. The option is activated in the top right corner of the 3d viewport in the mesh editing menu ([https://projects.blender.org/blender/blender/commit/4387aff  rB4387aff])
[[File:Correct Face Attributes and Keep Connected.png|thumb|center]]
[[File:Correct face attributes uv video.mp4|thumb|center|600px]]

* '''Edge Ring Select''': matching edit-mesh functionality of the same name using `Ctrl-Alt-LeftMouse` ([https://projects.blender.org/blender/blender/commit/1dd381828fa3f 1dd381828f]).

* '''Pick Shortest Path''': hold down `CTRL` and select UV components with `left mouse` to select the shortest path between the UV components. ([https://projects.blender.org/blender/blender/commit/ea5fe7abc183 ea5fe7abc1])
[[File:UV pick shortest path.mp4|600px|thumb|center]]

* '''Pick Shortest Path''': grid type selection if you activate '''fill region''' or use `Ctrl-Shift-LeftMouse` when selecting UV components
[[File:Pick shortest path fill region.mp4|600px|thumb|center]]

* '''UV Rip''': separates UV components (vertices, edges, faces) from connected components. The operator is run by pressing the `V` hotkey. The components are ripped in the direction of the mouse pointer position. ([https://projects.blender.org/blender/blender/commit/89cb41faa0596d898f850f68c46e2b39c25e6452  rB89cb41fa])

[[File:UV Rip.mp4|600px|thumb|center]]

* Option to change the opacity of the UV overlay. ([https://projects.blender.org/blender/blender/commit/90d3fe1e4d 90d3fe1e4d]).

* Remove selection threshold for nearby coordinates, only select when UV coordinates are identical. ([https://projects.blender.org/blender/blender/commit/b88dd3b8e7b b88dd3b8e7])

== Curves ==

* Curve Normals are disabled by default. ([https://projects.blender.org/blender/blender/commit/0bd7e202fbd6 0bd7e202fb])
* Curve Handles now can be Hidden/Selected or All. ([https://projects.blender.org/blender/blender/commit/49f59092e7c8 49f59092e7])
The new default for Handles is `Selected`. For Nurb Curves, the handlers are displayed always as it was in previous version.

[[File:GifCurvesAll.gif|600px|thumb|center|New Handle Display]]

== Transform ==

* Edge and Vert Slide now supports the other snapping types. ([https://projects.blender.org/blender/blender/commit/e2fc9a88bcb3 e2fc9a88bc], [https://projects.blender.org/blender/blender/commit/9335daac2a36 9335daac2a])
* The snap can now be made to the intersection between constraint and geometry. ([https://projects.blender.org/blender/blender/commit/d8206602feed d8206602fe])
[[File:Snap with Constraint.gif|600px|thumb|center|New behavior of the snap with constraint]]

== Modifiers ==
* The ocean modifier can now generate maps for spray direction. ([https://projects.blender.org/blender/blender/commit/17b89f6dacba 17b89f6dac])

[[File:Wave-modifier.jpg|800px|full|center]]

* Applying a modifier as a shapekey is now allowed even when the object has linked duplicates. ([https://projects.blender.org/blender/blender/commit/01c8aa12a1 01c8aa12a1])
* New menu option to Save As Shapekey, which applies the modifier as a shape key without removing it. ([https://projects.blender.org/blender/blender/commit/01c8aa12a1 01c8aa12a1])