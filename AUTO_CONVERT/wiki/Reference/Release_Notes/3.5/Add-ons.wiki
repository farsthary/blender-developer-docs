= Add-ons =

== Rigify ==
* Ported the ability to generate Action Constraint layers from Cloud Rig ([http://developer.blender.org/D16336 D16336], [https://projects.blender.org/blender/blender-addons/commit/dddf346f1adf  rBAdddf346]).

== Storypencil ==
*UI Refactor: New Mode selector and settings for more discoverability of the add-on two main modes (Switch and New Window).
*In Switch Mode the audio file in the video sequencer is copied temporarily to the source scene allowing to keep audio in sync. This can be disabled in the Storypencil settings panel.
*IN/OUT strip range markers ar now showed in Switch Mode.
*TAB key can be used now to edit the strip scene under the timeline cursor.
*Bugs Fixed. ([https://projects.blender.org/blender/blender-addons/commit/6fcd157f2497d9ba4ba82191cb2abf3de11a0394  rBA6fcd157])

[[File:Storypencil panel.png|285px]]

== Sun Position ==
* Add Show Surface and Show Analemmas options. These are useful to visualize the trajectory of the Sun in the sky during the year, for each hour of the day. ([https://projects.blender.org/blender/blender-addons/commit/29a67357a059  rBA29a6735])

== glTF 2.0 ==

=== Importer ===

* Custom primitive attributes are now imported as attributes ([https://projects.blender.org/blender/blender-addons/commit/4f8ccce4a5357974804dcecce88deddd67dcb8f3  rBA4f8ccce])
* Better uri management ([https://projects.blender.org/blender/blender-addons/commit/fc8bd7d6d4b7e0b34fb008ac328e839cca813094  rBAfc8bd7d])


=== Exporter ===

* Add an export identifier, usefull for glTF exporter called by another addon ([https://projects.blender.org/blender/blender-addons/commit/665a82e17100e4560ea0ef55a51f8721c670c936  rBA665a82e])
* Fix regression on Vertex Color export (face corner domain) ([https://projects.blender.org/blender/blender-addons/commit/d18a07f51404ff155de0c21ac1ea32e839e74d88  rBAd18a07f])
* Fix when custom shader node has no type ([https://projects.blender.org/blender/blender-addons/commit/23661bdd40144efb1f681396947c38cdecbe0163  rBA23661bd])
* Various fixed in shader node tree export ([https://projects.blender.org/blender/blender-addons/commit/5a774a6dad4378f173ec7fdcdcd406048fcc9a29  rBA5a774a6])
* Add option to choose jpeg quality ([https://projects.blender.org/blender/blender-addons/commit/bcc792388887d15bbed6f87842cf8fe6079f8323  rBAbcc7923])
* Manage future deprecation in numpy ([https://projects.blender.org/blender/blender-addons/commit/a131db64b9482cb6de52dc561b60f8decbe307a7  rBAa131db6])
* Export custom attribute only when start with underscore, to avoid exporing built-in attributes ([https://projects.blender.org/blender/blender-addons/commit/d5732014b103c7dd28e79bdc8e4bffc8201ba260  rBAd573201])
* Fix division by 0 when trans+spec ([https://projects.blender.org/blender/blender-addons/commit/2207539e32c34609c9c8ca48c640f91259e71326  rBA2207539])
* Better uri management ([https://projects.blender.org/blender/blender-addons/commit/fc8bd7d6d4b7e0b34fb008ac328e839cca813094  rBAfc8bd7d])
* For glTF separated mode, use tab as indent (save space) ([https://projects.blender.org/blender/blender-addons/commit/07157fbb8fe590b868ab2e65af04a5d723d4a45f  rBA07157fb])
* Better parent check, when bone parent is still set, but user moved to "OBJECT" parent type, without resetting bone parent ([https://projects.blender.org/blender/blender-addons/commit/bddbd5ed5fa6af76b50b4f9ac202cc2903977248  rBAbddbd5e])
* Update file filter in UI when user change the export mode (*.glb/*.gltf) ([https://projects.blender.org/blender/blender-addons/commit/38890b51d3c262cba7b8df1063a33ce89e558550  rBA38890b5])
* Fix check for node tree ([https://projects.blender.org/blender/blender-addons/commit/0554223a844549ec2e18462fa76e2c34fafe8f19  rBA0554223])
* Round normals to avoid vertex split ([https://projects.blender.org/blender/blender-addons/commit/48b114c17881a752d5dde1db68e77b49de063f2f  rBA48b114c])
* Fix variable name collision that leads to crash ([https://projects.blender.org/blender/blender-addons/commit/c6c45a843e761045ba755c12c6394437564485c8  rBAc6c45a8])
* Fix error after reloading scripts ([https://projects.blender.org/blender/blender-addons/commit/599f2c4fdac28c9cbb15704062b17e7b3358a57c  rBA599f2c4])
* Manage new mirror texture option ([https://projects.blender.org/blender/blender-addons/commit/69beaa0e43b244f8e74d5e53ce0fe34731ac0bb9  rBA69beaa0])
* Fix object parented to bone TRS ([https://projects.blender.org/blender/blender-addons/commit/d31fa71a6b27bcb168b46c44d22dfb075482591a  rBAd31fa71])
* Fix typo in tangent morph export ([https://projects.blender.org/blender/blender-addons/commit/afed066115d8102b41059df6ba9a2914ea3f4f70  rBAafed066])
* Add hook to change attribute ([https://projects.blender.org/blender/blender-addons/commit/d936a948e877dcb79817ab9f214514514540f6d5  rBAd936a94])

== Contrib repository ==
* Contrib add-ons are now already excluded in beta, to better communicate that releases aren't shipping with them. Also improve the UI to only show the relevant categories for releases. ([https://projects.blender.org/blender/blender/commit/e8c7866608bb  rBe8c78666])