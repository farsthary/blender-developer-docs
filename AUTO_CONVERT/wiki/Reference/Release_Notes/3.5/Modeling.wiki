= Modeling =

* A new '''Set Attribute''' operator allows basic control over generic attribute values in edit mode ([https://projects.blender.org/blender/blender/commit/6661342dc549a8492 6661342dc5]).
* A new '''Flip Quad Tessellation''' operator allows control over the tessellation orientation of quads ([https://projects.blender.org/blender/blender/commit/328772f2 328772f2]).

[[File:Flip Quad Tessellation.webm|thumb|center]]

* Edit mesh conversion to evaluated meshes has been parallelized and can be over twice as fast depending on the mesh and CPU ([https://projects.blender.org/blender/blender/commit/ebe8f8ce7197 ebe8f8ce71]).

== UV Editing ==
==== Copy/Paste ====
The most anticipated UV feature in this release is '''UV Copy''' and '''UV Paste'''.

For low poly modeling and game assets, you can now select connected faces, select from the menu ''"UV > Copy UVs"'', this will make a copy of the UVs in a separate clipboard.

Next, select different faces that have the same topology. They can be on a different mesh, a different UV channel, or even a different .blend file. Choose "''Paste UVs''", and the UVs will be pasted over. ([https://projects.blender.org/blender/blender/commit/721fc9c1c950 721fc9c1c9])


==== Improvements ====
The '''Constrain to Image Bounds''' feature received fixes for the '''Shear Operator''', and to improve robustness on unusual inputs.
With this change, '''all''' of the UV operators should now correctly support '''Constrain to Image Bounds''' and it can now be considered feature complete. ([https://projects.blender.org/blender/blender/commit/3d1594417b0e 3d1594417b] and [https://projects.blender.org/blender/blender/commit/0079460dc79a 0079460dc7])

UV '''Sphere Projection''' and UV '''Cylinder Projection''' now have better handling of UVs at the poles. In addition, you can now specify whether to "'''Fan'''" or "'''Pinch'''" the UVs. All the projection options are now fully supported in all modes. ([https://projects.blender.org/blender/blender/commit/280502e630e9 280502e630])


==== Fixes ====
* Bring back the missing UV overlay in '''Texture Paint Mode'''. ([https://projects.blender.org/blender/blender/commit/4401c93e452f 4401c93e45])
* '''Select Similar''' now has support for '''Similar Object''' and '''Similar Winding''' ([https://projects.blender.org/blender/blender/commit/2b4bafeac68e 2b4bafeac6])
* Fix '''UV Unwrap''' for quads with '''Reflex Angles'''. ([https://projects.blender.org/blender/blender/commit/f450d39ada1f f450d39ada])
* Fix '''UV Unwrap''' broken for n-gons with 3 shared vertices. ([https://projects.blender.org/blender/blender/commit/644afda7eb6c 644afda7eb])