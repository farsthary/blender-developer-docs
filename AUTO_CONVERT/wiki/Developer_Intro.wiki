=New Developer Introduction=


<div class="bd-lead">

Getting started with Blender development

</div>


* [[Developer_Intro/Overview|New Developer Overview]]
* [[Developer_Intro/Advice|New Developer Advice]]
* [[Developer_Intro/Environment|New Developer Environment]]
* [[Developer_Intro/Committer|New Developer Commit Access]]
* [[Developer_Intro/Resources|New Developer Resources]]
* [https://projects.blender.org/blender/blender/issues?labels=302 Good First Issue]