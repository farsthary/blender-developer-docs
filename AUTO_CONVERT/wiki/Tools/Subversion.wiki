=Subversion=

The Blender source code and add-on repositories have moved to Git, however some things remain in Subversion repositories for now:
* Prebuilt library dependencies
* Test files

Repositories from old projects on projects.blender.org are also still accessible through svn.

==Repository access==

Access to the repositories is provided via https:// both for anonymous (read access) and commit access (write access).

'''Windows, Linux and macOS libraries''':
* SVN URL: https://svn.blender.org/svnroot/bf-blender/trunk/lib/<operating_system>

'''Tests''':
* SVN URL: https://svn.blender.org/svnroot/bf-blender/trunk/lib/tests

==Read Access==

The source code can be checked out anonymously.

You will need a Subversion client. Most systems have one installed by default. For a list of clients see [http://subversion.apache.org/packages.html Overview of Subversion clients].

When you checkout, you will probably see an error message about the certificate that is not issued by a trusted authority. However, the certificate is fully functional and will ensure secure transit of the data. You can permanently accept this certificate.

If you get the message "Certificate verification error: signed using insecure algorithm" without proposing to accept the certificate, open with the file `~/.subversion/servers` and add on the bottom of it:
 `ssl-trust-default-ca = no`


==Commit Access==

You need to generate a token that you can use as a password, as follows:
* Got to [https://projects.blender.org/user/settings/applications Settings > Applications] on projects.blender.org
* Under Generate New Token, set Token Name to "subversion"
* Under Select Permissions, enable the "Read" permission for "user" (at the very end of the list) 
* Click Generate Token

When committing, you will be prompted for a username (same as on projects.blender.org) and password (token).

Subversion will store your password on your local system and use it next time you communicate with the repository.

If you forget your password, you can delete the token and generate a new one.

==Additional information==

* [http://subversion.apache.org/packages.html Overview of Subversion clients]
* [http://svnbook.red-bean.com/ Version Control with Subversion] (online book)
* [http://subversion.apache.org/ Subversion's website]