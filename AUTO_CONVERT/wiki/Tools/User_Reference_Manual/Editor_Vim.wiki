= Editing reStructuredText in Vim =

This page contains additions you can make to your <tt>vimrc</tt>

To avoid conflict with any of your preferred settings for other file-types: <tt>"autocmd FileType"</tt> is used where possible.

<source lang=vim>
" Example '.vimrc'

" needed to detect '.rst' files for special handling.
filetype plugin on

" indent based on filetype
filetype indent on


" These settings will only impact editing '.rst' files.
" so as not to conflict with any of your other settings.

" indentation settings
autocmd FileType rst setlocal expandtab shiftwidth=3 tabstop=3

" show a ruler at the right hand margin
autocmd FileType rst setlocal colorcolumn=120

" for wrapping text
autocmd FileType rst setlocal textwidth=118

" set spelling
autocmd FileType rst setlocal spell spelllang=en_us

" enable syntax highlighting
autocmd FileType rst syntax on

" all files use utf-8 encoding
autocmd FileType rst setlocal encoding=utf-8


</source>