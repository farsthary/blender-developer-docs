=Translation=


<div class="bd-lead">

Information about the internationalization of Blender's UI.

</div>



* [[Process/Translate_Blender]]
* [[Source/Interface/Internationalization]]

The Blender user manual translation is documented on [https://docs.blender.org/manual/en/dev/about/contribute/index.html#translations the manual itself].