= Building Blender on Windows =


<div class="bd-lead">

These are instructions to build Blender for Windows, with Microsoft Visual Studio.

</div>

== Quick Setup ==

Building Blender is not as hard as most people would think. For beginners, easiest is to follow these steps carefully. For more details and alternative ways to set up a build environment, see below.

=== Install Development Tools ===

Subversion, Git, CMake and Visual Studio '''must all be installed'''.

* Install [https://visualstudio.microsoft.com Visual Studio 2019 or 2022 Community Edition] (free, be sure to install the 'Desktop Development with C++' workload)
* Install a Subversion client, such as [https://tortoisesvn.net/downloads.html TortoiseSVN]
** In the installer, enable '''Command Line Client Tools'''.
* Install [https://gitforwindows.org/ Git for Windows]
** In the installer, choose to '''add Git to your PATH''' to ensure `make update` can correctly function.
* Install [https://cmake.org CMake]
** In the installer set the system path option to '''Add CMake to the system PATH for all users'''.

=== Starting the command prompt ===

Then open the command prompt window by hitting the Windows key+R, and then typing cmd.exe

''It is important that you use '''cmd.exe''' and not powershell or any other shell, the instructions below may not work otherwise.''

=== Download Sources and Libraries ===

Create a folder to store your copy of the Blender source code. This guide will assume your chosen folder is <code>C:\blender-git</code>.

Download the Blender source code:
<source>
cd C:\blender-git
git clone https://projects.blender.org/blender/blender.git
</source>

Download the Blender libraries:

<source>
cd C:\blender-git\blender
make update
</source>

make will automatically detect the libraries you need and offer to download them for you. Do note that this set of libraries is several gigabytes large and may take a while to download.  

If you prefer to download the libraries manually please see the advanced setup section below

=== Compile Blender ===

<source>
cd C:\blender-git\blender
make
</source>

When using Git Bash (or similar) instead of the Windows command prompt, use <tt>./make.bat</tt> instead of <tt>make</tt>.

Once the build finishes you'll get one or more message like this, pointing to the location of your freshly built Blender:
`-- Installing: C:\blender-git\build_windows_Full_x64_vc14_Release\bin\Release\[somefile]` 

in that folder you should find your `blender.exe` you can start to run blender.

To update to the latest changes afterwards, run:
<source>
cd C:\blender-git\blender
make update
make
</source>

== Branches ==

With the quick setup instructions the `main`  branch will be built by default, which contains the latest Blender version under development.

To build another branch or a [[Tools/Pull_Requests|pull request]], first checkout the branch and then run `make update` to update the add-ons, libraries and tests to match.

<source>
cd C:\blender-git\blender
git checkout <branch-name>
make update
make
</source>

= Advanced Setup =

== Development Tools ==

=== Visual Studio ===

We support Visual Studio 2019 16.9.16 and VS2022. Older versions will not work. Generally, newer builds will result in better Blender performance. The Visual Studio Community editions are free for open source development and have all the features needed for building and developing Blender. Professional editions work fine as well.

=== Subversion ===

Subversion is required for accessing precompiled libraries. The command line [http://www.sliksvn.com/en/download SlikSVN] is sufficient, but alternatively [https://tortoisesvn.net/ TortoiseSvn] can be used for an easy to use GUI, with Windows Explorer integration.

=== Git ===

Git is required for accessing the Blender source code. [https://gitforwindows.org/ Git for Windows] command line utilities are sufficient, but more user friendly user interfaces exist. For example [http://tortoisegit.org/ TortoiseGit] is the equivalent of ToroiseSvn with a GUI and Window explorer integration.

''For additional information on using Git with Blender's sources, see: [[Tools/Git|Tools/Git]]''

== Download Sources and Libraries ==

The easiest method is to follow the quick command line steps above, to ensure everything is downloaded exactly as needed.

Alternatively GUI tools may be used to checkout and update the repositories manually. The Blender build system expects the following layout within a folder:

<source>
blender/           # Blender source code from Git, git://git.blender.org/blender.git
lib/win64_vc15     # Precompiled 64-bit libraries from Subversion, https://svn.blender.org/svnroot/bf-blender/trunk/lib/win64_vc15
</source>

== Compile Blender ==

=== Automatic CMake Setup ===

If you're not interested in manually setting up CMake build directory, configuring, building and installing in separate steps, we provide a convenience batch file in Blender's source directory which sets up CMake for you.

To update to the latest version and build, use:
<source>
cd C:\blender-git\blender
make
</source>
''(this requires git and svn to be in your systems PATH).''

By default Blender builds will be similar to official releases. Many [[Building_Blender/Options|Build Options and Targets]] are available for debugging, faster builds, and to enable or disable various features.

For a full list of the optional targets type...
<source>
make help
</source>

=== Building from within the Visual Studio IDE ===

If you want to work within the visual studio IDE instead of building from the command prompt. 

We provide a convenience batch file in Blender's source directory which can generate a visual studio project for you.

From the command line, run:

<source>
cd C:\blender-git\blender
make full nobuild
</source>

If you want to customize your build (i.e. select a visual studio version, platform architecture, of blender feature set), you can get a list of customizable options by typing:

<source>
make help
</source>

Once the batch file finishes it should tell you where the project files have been written, for example:

<source>
-- Build files have been written to: c:/blender-git/build_windows_Full_x64_vc14_Release
</source>

In that folder you should now find a blender.sln which you can open with the visual studio IDE. 

Once opened it's very important the following steps are taken:

* In the solution explorer find the '''INSTALL''' project, right click it and select 'Build' this will copy all the required files the the blender output folder. NOTE: This needs to be done atleast *once* for every solution configuration, ie: if you switch from Debug to Release configuration you need to redo this step. 

Once these settings are done, you can work with the code as usual in visual studio.

=== Manual CMake Setup ===

If you want to have more control over your build configuration or have multiple build directories for a single source directory.
You can follow these steps.

==== Create Project Files ====

This step you only have to do once when first setting up the build.

Install CMake and run the CMake application, you will have to point its source path to the location you downloaded blenders GIT source to.

You will need to select a '''Build Path''' outside the source directory.

Press Configure, then Generate.

''Note. any changes to the build-system will re-generate project files automatically from within MSVC when building.''

==== Compile from Visual Studio IDE ====

''Note that if you are already familiar with Visual Studio, Then the following steps are are much the same as building any other project file (nothing Blender specific here).''

* From Visual Studio you need to load the project file '''Blender.sln''' from the '''Build Path''',
* Select the menu item: Build -> Build Solution ''(this will take some minutes)''

* Build the '''INSTALL''' target listed in the '''Solution Explorer'''.<br>Note: the '''INSTALL''' copies DLL's and files used by Blender to the build directory so <tt>blender.exe</tt> can start.<br>If you skip this step running <tt>blender.exe</tt> will fail to start, reporting missing DLL files.<br>You wont have to run this target every build, but do it after updating SVN libraries, otherwise you risk not having the correct libraries to run blender.

==== Compile from the command line  ====

You can also compile from the command line, from the '''Build Path''' folder
  
In the following command just substitute [CONFIG] with one of the following options:<br><tt>Debug, Release, RelWithDebInfo, MinSizeRel</tt>.

<source>
cmake --build . --target INSTALL --config [CONFIG]
</source>

== Build with Cycles GPU Binaries ==

See here for the [[Building_Blender/GPU Binaries|instructions on building CUDA, Optix, HIP, OneAPI support]].