= Cycles Renderer =

For source code and contact details, see the [https://developer.blender.org/project/manage/26/ Cycles project page on developer.blender.org].

The [https://developer.blender.org/project/board/99/ workboard] has an overview of ongoing and planned tasks.

== Design ==

* [[Source/Render/Cycles/DesignGoals|Design Goals]]
* [[Source/Render/Cycles/NodeGuidelines|Node Guidelines]]

== Code ==

* [[Source/Render/Cycles/SourceLayout|Source Layout]]
* [[Style_Guide/C_Cpp|Style Guide]]
* [[Source/Render/Cycles/Standalone|Cycles Standalone]]

== Implementation ==

==== Scene Graph ====
* [[Source/Render/Cycles/SceneGraph|Scene Graph]]
* [[Source/Render/Cycles/BVH|BVH]]

==== Device Abstraction ====
* [[Source/Render/Cycles/Devices|Devices]]
* [[Source/Render/Cycles/Kernel/Language|Kernel Language]]

==== Scheduling ====
* [[Source/Render/Cycles/RenderScheduling|Render Scheduling]]
* [[Source/Render/Cycles/Kernel/Scheduling|Kernel Scheduling]]
* [[Source/Render/Cycles/MultiDeviceScheduling|Multi Device Scheduling]]

==== Output ====
* [[Source/Render/Cycles/Tiling|Tiling and OpenEXR Cache]]
* [[Source/Render/Cycles/Drivers|Output and Display Drivers]]

== Algorithms ==

* [[Source/Render/Cycles/SamplingPatterns|Sampling Patterns]]
* [[Source/Render/Cycles/Closures|Closures]]
* [[Source/Render/Cycles/Volumes|Volumes]]
* [[Source/Render/Cycles/LightSampling|Light Sampling]]
* [[Source/Render/Cycles/Units|Units & Colors]]
* [[Source/Render/Cycles/LightLinking|Light and Shadow Linking]]

== Outdated ==

To be updated or removed.

* [[Source/Render/Cycles/Threads|Threads]]
* [[Source/Render/Cycles/Papers|Papers]]
* [[Source/Render/Cycles/Optimization|Optimization Ideas]]
* [[Source/Render/Cycles/Network_Render|Network Render]]
* [[Source/Render/Cycles/Volume|Volume Render]]