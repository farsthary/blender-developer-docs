= Sprint 11 =

* '''Start''': 2021/03/08
* '''End''': 2021/03/19

Note: ''The team will meet in the middle of the sprint (Monday) to check if the sprint backlog is still valid''.

* '''Goal''': Finish the core of spreadsheet for 2.93, primitive nodes and prototypes for curtain node and "tools" integration with nodes.

== Time ==

* Jacques: 8 days
* Pablo: 2 day
* Hans: 8 days
* Simon: 2 day

== Impediments ==

== Sprint Review ==

=== What went well ===

* Prototype development went quite well - quick iterations, quickly checking if the design works or not.

* Focus on polishing (interpolation for attributes) helped to solidify the foundations.

* The release cycles did not get on the way of the team pace and well being.

* The slicing of features in added value good enough for a release is working well.

* Time management (meetings didn't go past 14:30 any day).

* A lot of community patches are being considered/added.

=== What didn't go well ===

* Design meetings were longer than needed to be - either by the lack of agenda or focus.

* Discussions on tools and prototypes were a bit hard on Hans. It wasn't clear on the get go that the team is not committed to build those solutions but to explore them at the design level first.

* Too much design decisions are being confined in the team without being communicated to the outside world.

* Simon feels the amount of prototypes may be too much in detriment to polishing what we know may already benefit some of the same problems we are trying to solve with the prototypes.

* It is hard to get the feedback and design pitches from Ton.

=== Improvements ===

* Dalai to try to use the meeting room for feedback sections with Ton.

* Use hackmd for design meetings for ongoing notes.

* Document design discussions on the wiki.