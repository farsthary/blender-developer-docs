= Steam Release =

{|class="note"
|-
|<div class="note_title">'''NOTE'''</div>
<div class="note_content">This procedure has changed to use Buildbot. Needs updating.</div>
|}



Use the script '''`create_steam_builds.py`''' in `release/steam` (apply [https://developer.blender.org/D8429 D8429] until it has landed). The necessary IDs for application and depots can be found in the shared credential manager under '''Development > Steam App and Depot IDs'''.

'''Note''' that at the moment it is possible to use the script only on MacOS, until a good way of unpacking the DMG archive on other platforms exists.

When the build has completed log in to the [https://partner.steamgames.com/apps/builds/365670 application page] and '''assign''' the new build to the appropriate branches.

* If you are doing an LTS release assign it to the LTS branch, i.e. `2.83lts`. If you are doing an LTS for a version that doesn't have a branch yet, create it first.
* The `default` branch will always have the latest stable release. If the latest LTS release is _also_ the latest release in general (i.e. 2.83 is the very latest official release), then assign the LTS release to both its LTS branch and the `default` branch. If the LTS isn't the latest release, it goes only to its LTS branch. For instance 2.90.0 goes into `default`, and `2.83.5` goes into `2.83lts`.


== Event ==

We also create an announcement on the steam platform to deploy the build - done on https://steamcommunity.com/app/365670, then click the Hub Admin button, Post New Announcement. To be coordinated with the rest of the release PR. In general you'll get copy from PR, but below is some template in case you don't.

'''Title''': ''"Blender 2.83 Official Release is available"''

'''Second title''': ''<one-line teaser from release notes>''

'''Text''':

<source>
<initial paragraph from release notes> Read more about the full feature set on <a href="https://www.blender.org/download/releases/2-82/">blender.org</a>.

<IMAGE SCREENSHOT>

If you want to support Blender development, consider joining the <a href="https://fund.blender.org/">Blender Development Fund</a>.
</source>

'''Images''':
* Screenshot - delete `recent-files.txt`; run with `-p 0 0 1920 1080 --factory-startup` and screenshot without the window border, to be used in the text where `<IMAGE SCREENSHOT>`

* Splashscreen - the splash_2x.png file converted to jpg.

* Cover Image / Banner - 800x450 for event
* Header Image - 1920x622 for event
* Library Spotlight Banner - 2108x460 for event


=== Update Store Graphics ===

Note: images have maximum allowed file size of 5120Kb.

Under Store Page Admin > [https://partner.steamgames.com/admin/game/edit/34584?activetab=tab_graphicalassets Graphical Assets]

==== Store Assets ====
* From splash screen:
** 460px x 215px for Header Capsule Image, named `header.jpg`
** 616px x 353px for Main Capsule Image, name `capsule_616x353.jpg`

==== Library Assets ====
* Detail from banner with Blender logo overlayed
** 600px x 900px for Library Capsule Image named `library_600x900.jpg`
** 3840px x 1240px for Library Hero Image named `library_hero.jpg`