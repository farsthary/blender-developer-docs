=Google Summer of Code - 2019 Projects=

<div class="bd-lead">

Progress updates for 2019
</div>


==Current State==
Results for the projects have been announced [https://code.blender.org/2019/09/google-summer-of-code-2019/ here].

'''1<sup>st</sup> evaluation - ''' 24 - 28 June<br/>
'''2<sup>nd</sup> evaluation - ''' 22 - 26 July<br/>
'''Final submission - ''' 19 - 26 August

==Communication==
===Development Forum===
All Blender and code related topics will go to the devtalk forum. Here students will also post their weekly reports, and can ask for feedback on topics or share intermediate results with everyone.

https://devtalk.blender.org/c/blender/summer-of-code

===blender.chat===
For real-time discussions between students and mentors.
Weekly meetings will be scheduled here as well. 

https://blender.chat

==Projects==
Google has granted 8 projects for the Blender Foundation.


----
===Continued development on LANPR engine for Blender===
by '''Yiming Wu'''<br/>
''Mentors:'' Sebastian Parborg, Clement Foucault

* [[User:Yiming/GSoC2019/Proposal|Proposal]]
* [[User:Yiming/GSoC2019/Updates|Updates]]
* [[User:Yiming/GSoC2019/LANPR_GSoC_2019_Summary|Summary]]

----

===Intel Embree BVH for GPU===
by '''Quentin MATILLAT'''<br/>
''Mentors:'' Sergey Sharybin, Stefan Werner


----
===Improve Cycles/EEVEE For Procedural Content Creation===
by '''Omar Ahmad'''<br/>
''Mentors:'' Jacques Lucke, Brecht Van Lommel

* [[User:OmarSquircleArt/GSoC2019/Proposal|Proposal]]

----

===Outliner Improvements===
by '''Nathan Craddock'''<br/>
''Mentors:'' Campbell Barton, Brecht van Lommel

* [[User:Zachman/GSoC2019/Proposal|Proposal]]
* [[User:Zachman/GSoC2019/Plan|Plan and Schedule]]
* [[User:Zachman/GSoC2019/Report|Final Report]]


----

===Core Support of Virtual Reality Headsets through OpenXR===
by '''Julian Eisel'''<br/>
''Mentors:'' Dalai Felinto, Sebastian Koenig

* [[User:Severin/GSoC-2019/Proposal|Proposal]] (Updated)
* [[User:Severin/GSoC-2019/How_to_Test|How to Test]]
* [[User:Severin/GSoC-2019/Final_Report|Final Report]]


----

===Cloth Simulator Improvement===
by '''Ish Hitesh Bosamiya'''<br/>
''Mentors:'' Brecht Van Lommel, Jeroen Bakker

* [[User:Ishbosamiya/GSoC2019/Proposal|Proposal]]
* [https://devtalk.blender.org/t/gsoc-2019-cloth-simulator-improvement-weekly-reports/7726?u=ish_bosamiya Weekly Reports]
* [[User:Ishbosamiya/GSoC2019/FinalReport|Final Report]]

----

===Fast Import / Export for OBJ, STL and PLY formats===
by '''Hugo Sales'''<br/>
''Mentors:'' Jacques Lucke, Sybren Stüvel

* [[User:Someonewithpc/GSoC2019/Proposal|Proposal]]


----
===Bevel Custom Profiles===
by '''Hans Goudey'''<br/>
''Mentors:'' Howard Trickey, Rohan Rathi
* [[User:HooglyBoogly/GSoC2019/Proposal|Proposal]]
* [https://devtalk.blender.org/t/gsoc-2019-bevel-profiles-weekly-reports/7651/21 Weekly Reports]
* [[User:HooglyBoogly/GSoC2019/Log|Log]]
* [[User:HooglyBoogly/GSoC2019/Notes|Notes]]
* [[User:HooglyBoogly/GSoC2019/Final_Report|Final Report]]