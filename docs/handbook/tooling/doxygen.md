# Doxygen

Blender uses [doxygen](http://www.doxygen.nl) for C/C++ source code
docs, you will need to install this first.

## Building

We've included a convenience build target.

``` bash
make doc_doxy
```

This is just a convenient way to run:

``` bash
cd doc/doxygen
doxygen Doxyfile
```

### Single File Builds

The complete documentation can take a while to build, to build docs for
a single file, we have included a utility.

While this misses cross-file references, it's useful for a quick
preview.

Example use:

``` bash
./source/tools/utils_doc/doxygen_single_file --browse doc/doxygen/Doxyfile /path/to/source.c
```

Where `/path/to/source.c` is the source file you're previewing.
`--browse` will open up the documentation in a web browser and can be
omitted if you prefer to do this manually.
