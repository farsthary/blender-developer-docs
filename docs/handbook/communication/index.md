# Communication

## Channels

- [Chat](chat.md)
- [Forums](forums.md)
- [Meetings](meetings.md)
- [Weekly Updates](weekly_updates.md)
- [User Feedback](user_feedback.md)

## Guidelines

- [Code of Conduct](code_of_conduct.md)
- [Copyright Rules](copyright_rules.md)
