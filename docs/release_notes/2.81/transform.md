# Blender 2.81: Transform & Snapping

## Transform

### Affect Origins

It's now possible to move object origins directly.

This is an alternative method of adjusting the origin, previously only
possible placing the cursor and setting an objects origins to the
cursor.

This only works for objects with data which can be transformed.

This is accessible from the Options panel.

### Affect Parents (Skip Children)

It's now possible to transform parents without affecting their children.

This is accessible from the Options panel.

### Mirror

Mirroring over the Y and Z axis is now supported, along with the
existing X axis option. Multiple axes can also be enabled
simultaneously.
(blender/blender@3bd4f22)

![Mirror now has X, Y and Z options](../../images/Transform_mirror.png){style="width:600px;"}

## Snap

### Edge Snapping Options

Two new snap options have been added:

- Edge Center, to snap to the middle of an edge.
  (blender/blender@5834124);
- Edge Perpendicular, to snap to the nearest point on an edge.
  (blender/blender@dd08d68);

|Snap modes Edge Center and Perpendicular|Edge Perpendicular snapping|
|-|-|
|![](../../images/Snap_Edge_Center_and_Perpendicular.png){width="293"}|![](../../images/Snap_Perpendicular.png){width="285"}|

### Auto Merge

When auto merging vertices, adjacent edges and faces can now be
automatically split to avoid overlapping geometry. This is controlled by
the new "Split Edges & Faces" option for Auto Merge.
(blender/blender@6b189d2)

![Auto merge and split faces](../../images/Blender2.81_auto_merge_split.png)
