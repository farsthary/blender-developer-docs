# Blender 2.93: Add-ons

## BlenderKit

![BlenderKit asset card](../../images/BlenderKit_add-on_Asset_card.jpg){style="width:500px;"}

- Right-click menu was replaced with the new asset card. The asset card
  shows all information about the asset and allows to do all operations
  previously available in the menu.
- Tooltip preview was simplified, the thumbnail can be seen unobstructed
  during drag-drop interaction.
- Ratings are shown more prominently and the results are shown always
  when at least one rating is available.
- Choose resolution before downloading an HDR.
- Upload UI was improved to be simpler.
- Rerender thumbnail for models - this command downloads the asset,
  renders thumbnail in the background, and reuploads it - great for
  beautifying your older thumbnails.

## Collection Manager

![Collection Manager Popup with new undo/redo buttons, object selection buttons, and improved indicators.](../../images/Collection_Manager_2.93_Release_Notes.png){style="width:450px;"}

### New Features

- Undo/Redo support was added to the Collection Manager Popup via undo
  and redo buttons near the top.
  (blender/blender-addons@88db9c67)
- Objects can now be selected, based on collection, in the Collection
  Manager Popup, and the selection/object state indication per
  collection has been improved.
  (blender/blender-addons@6dfba915)
  (blender/blender-addons@ebe76f3a)

### Bug Fixes

- Fixed a bug where QCD renumbering would add a 21st slot.
  (blender/blender-addons@8ea89b89)
- Fixed a bug where Phantom Mode was not disabling all of the UI
  elements it should have in the Collection Manager Popup.
  (blender/blender-addons@86194403)
- Fixed tooltip issues.
  (blender/blender-addons@df01c14f)
