# Blender 4.1: Add-ons

## 3DS I/O

- Export auto smooth angle from ´Smooth by Angle ´modifier.
  (blender/blender-addons@9a9142f160)

## FBX I/O

- Normals are now exported as vertex normals when the
`Mesh.normals_domain` property is `'POINT'`.
  (blender/blender-addons@3d823efc57)
- Normals are now exported using the `IndexToDirect` FBX reference
  mode.
  (blender/blender-addons@791b042c3a)
  - Older versions of the FBX I/O add-on do not support importing vertex
    normals with this reference mode, but will still import FBX I/O
    exported meshes with the correct normals because the vertex normals
    that cannot be imported will match the normals of the imported mesh.
    Custom normals won't be set in this case.

## Rigify

- Nested bone collections are now supported by the Rig Layers UI: when a parent
  collection is hidden, buttons for its children are greyed out.
  (blender/blender-addons@69f9e45f7b439)
- Some custom properties now use recently added boolean (checkbox/toggle) and enum (dropdown) types. (blender/blender-addons@0585a98f16c039cd6, [Manual](https://docs.blender.org/manual/en/4.1/addons/rigging/rigify/rig_features.html#limbs))
