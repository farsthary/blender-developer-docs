# Blender 4.1 Release Notes

Blender 4.1 is currently in **Beta**. Phase **Bcon3** until March 13,
2024.
[See schedule](https://projects.blender.org/blender/blender/milestone/18).

Under development in [`blender-v4.1-release`](https://projects.blender.org/blender/blender/src/branch/blender-v4.1-release).

* [Animation & Rigging](animation_rigging.md)
* [Core](core.md)
* [EEVEE & Viewport](eevee.md)
* [Grease Pencil](grease_pencil.md)
* [Modeling & UV](modeling.md)
* [Nodes & Physics](nodes_physics.md)
* [Pipeline, Assets & I/O](pipeline_assets_io.md)
* [Python API & Text Editor](python_api.md)
* [Rendering](rendering.md)
* [Cycles](cycles.md)
* [Sculpt, Paint, Texture](sculpt.md)
* [User Interface](user_interface.md)
* [VFX & Video](vfx.md)
* [Add-ons](add_ons.md)

## Compatibility
