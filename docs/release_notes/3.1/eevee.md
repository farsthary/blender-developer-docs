# Image editor

- Image editor is able to handle large images.
  (blender/blender@bdd74e1e9338)
- Tearing artifacts have been fixed on selected platforms.
  (blender/blender@bdd74e1e9338)
