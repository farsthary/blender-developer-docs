# Blender 3.0: Animation & Rigging

- New keying set: **Location, Rotation, Scale, and Custom Properties**
  (blender/blender@7192ab614b49).
  Effectively, it combines the behaviour of *Whole Character (selected
  bones only)* (which keys loc/rot/scale/customprops, but only works in
  pose mode) with *Location, Rotation, and Scale* (which works in both
  object and pose mode, but doesn't key custom properties). This makes
  it possible to use one keying set for animating both characters and
  props.
- Ctrl+F in animation editors no longer blocks the UI with a popup, but
  simply shows & activates the channel search textbox
  (blender/blender@41a4c62c3155).
- FCurve and NLA **modifier properties can now be overridden**
  (blender/blender@d2311de21853).
  Previously an armature/object with overrides could get new FCurve/NLA
  modifiers, but the properties would be read-only. Now they can be
  edited.
- Keyframe removal (Default: Alt+I, Industry Compatible: Alt+S) now
  respects the active keying set
  (blender/blender@7fc220517f87,
  blender/blender@1364f1e35c20).
- Custom bone shapes now have full translation/rotation/scale options
  (blender/blender@fc5bf09fd88c).

![](../../images/release-notes-300-animation-rigging-custom-bone-transform.png){style="width:300px;"}

- The Asset Browser now supports rendering previews for Action
  datablocks
  (blender/blender@17534e28ff44).
  This is documented further in the [3.0 Asset
  Browser](asset_browser.md) release
  notes.
- FCurves and all their keys can be selected by box- or circle-selecting
  the curve itself.
  (blender/blender@2246d456aa84)
  - Box selecting a curve selects all the keyframes of the curve.
  - Ctrl + box selecting of the curve deselects all the keyframes of the
    curve.
  - Shift + box selecting of the curve extends the keyframe selection,
    adding all the keyframes of the curves that were just selected to
    the selection.
  - In all cases, if the selection area contains a key, nothing is
    performed on the curves themselves (the action only impacts the
    selected keys).

<figure>
<video src="../../../videos/release-notes-300-animation-rigging-fcurve-select.mp4" width="600" controls=""></video>
</figure>

- Make Single User: in addition to **object** animation, now **object
  data** (mesh, curve, ...) animation can be made single user as well.
  (blender/blender@d0c5c67e940b)
- FCurve modifiers are now correctly evaluated in Restrict Range Borders
  (blender/blender@9dee0a10c81d).
  - When using FModifier Restrict Frame Range, the resulting influence
    was zero being exactly on Start / End range borders (so borders were
    exclusive). This made it impossible to chain FModifers together
    (forcing the user to specify values slightly below the desired
    border in following FModifiers). This is now corrected to be
    inclusive on Start / End range borders.

## Motion Paths

New operator: **Update All Visible Motion Paths**
(blender/blender@4de0e2e7717f).
Besides having this handy new button, there is also a smaller, related
improvement that updating the motion paths for an armature now updates
all the bones motion paths simultaneously.

![](../../images/release-notes-300-animation-motion-paths-refresh-all.png){style="width:264px;"}

## Vertex Groups

- The vertex group names are now stored in meshes directly instead of
  objects, which causes various compatibility issues with files from
  previous versions. See the [compatibility
  section](core.md#compatibility) in
  the core release notes for more details.
  (blender/blender@3b6ee8cee708,
  blender/blender@fc32567cdaa5)

## Pose Library

A new Pose Library has been added to Blender
(blender/blender@9473c61b366,
blender/blender@28dc07a153d,
blender/blender@f3610a23d12,
blender/blender@e01b52100f).
It is based on the [Asset
Browser](asset_browser.md). For demo
videos & a thorough explanation, see the [Pose Library
v2.0](https://code.blender.org/2021/05/pose-library-v2-0/) blog post.
The pose library is partially implemented as add-on (enabled by default;
blender/blender@34771cc9f56)
such that studios can create their own alternative implementations.

### Converting poses from the old to the new pose library

![](../../images/release-notes-300-animation-pose-library-convert.png){style="width:301px;"}

To convert an old-style pose library to a new one, follow these steps:

1.  Make sure the active object is the character rig.
2.  In the Action Editor, select the pose library Action.
3.  In the sidebar (press N if not visible), choose "Convert Old-Style
    Pose Library".

This will create an Action asset for each pose in the library.

## Bendy Bones

![](../../images/release-notes-300-animation-rigging-bbone-scale.png){style="width:301px;"}

- Renamed confusingly named Curve Y and Scale Y channels to Z. Animation
  curves and drivers are automatically updated, but Python scripts have
  to be changed manually.
  (blender/blender@682a74e0909)
- Added actual Scale Y channels that produce non-uniform segment
  lengths.
  (blender/blender@638c16f4101).
- Added simple toggles that replace up to 6-8 trivial drivers copying
  handle bone local scale to the corresponding properties.
  (blender/blender@b6030711a24)

!!! note "Forward Incompatibility Breakage"
    Blend files saved in Blender 3.0 with these features can cause
    unwanted behavior in older versions of Blender, see [#89621](http://developer.blender.org/T89621).

## Constraints

- New "Local Space (Owner Orientation)" choice for Target Space of bone
  targets, which allows copying local transformation while adjusting for
  the difference in rest pose orientations aiming to produce the same
  global motion.
  (blender/blender@5a693ce9e3)

<figure>
<video src="../../../videos/Demo-owner-local.mp4" width="600" controls=""></video>
</figure>

- Constraints now have Apply Constraint, Duplicate Constraint, and Copy
  To Selected operators in an "extras" menu, similar to modifiers
  (blender/blender@d6891d9bee2).

![](../../images/Release_Notes-Blender-3.0-Constraints-extras-menu.png){style="width:347px;"}

### Limit Rotation

- The constraint now correctly removes shear before processing, and
  without any limits can be used for that explicit purpose.
  (blender/blender@edaaa2afddb2)
- Added an Euler Order option similar to Copy Rotation.
  (blender/blender@d2dc452333a4)

### Copy Transforms

- New Remove Target Shear option for removing shear from the result of
  the Target Space transformation.
  (blender/blender@bc8ae58727)
- More Mix mode options representing different ways to handle scale and
  location, resulting in a complete set of six "Before/After Original
  (Full/Aligned/Split Channels)" choices in addition to Replace.
  (blender/blender@bc8ae58727)

<figure>
<video src="../../../videos/Demo-mix-full-aligned-split1.mp4" width="600" controls=""></video>
</figure>

### Action

- More Mix mode options to complete the same set of six "Before/After
  Original (Full/Aligned/Split Channels)" choices as in Copy Transforms.
  (blender/blender@cf10eb54cc)
- For constraints on *bones*, the default Mix mode is now "Before
  Original (Split Channels)".
  (blender/blender@cf10eb54cc)

### Stretch To

- The default Rotation Type for newly created constraints has been
  changed to Swing, which was introduced in 2.82 to replicate the
  behavior of the common Damped Track + Stretch To combination using
  just the Stretch To constraint.
  (blender/blender@8da23fd5aaa)

## Pose Sliding / In-Betweens Tools

<figure>
<video src="../../../videos/release-notes-300-animation-rigging-pose-sliding-tools.mp4" width="600" controls=""></video>
</figure>

These "In Betweens" tools have been improved
(blender/blender@9797b95f6175):

- Push Pose from Rest Pose
- Relax Pose to Rest Pose
- Push Pose from Breakdown
- Relax Pose to Breakdown
- Pose Breakdowner

These all now use the same new sliding tool:

- Actual visual indication of the blending/pushing percentage applied.
- Mouse wrapping to allow for extrapolation without having to worry
  about the initial placement of the mouse. This also means these tools
  are actually usable when chosen from the menu.
- Precision mode by holding Shift.
- Snapping to 10% increments by holding Ctrl.
- Overshoot protection; by default the tool doesn't allow overshoot
  (lower than 0% or higher than 100%), and it can be enabled by pressing
  the E key.
- Bones are hidden while sliding, so the pose itself can be seen more
  clearly. This can be toggled by pressing the H key while using the
  tool.

New Pose Sliding Operator - Blend To Neighbour
(blender/blender@f7a492d46054):

- Nudge the current pose to either the left or the right pose.
- Useful for dragging parts in an inbetween without losing your current
  pose.
- Found in Pose Mode under <span class="literal">Pose</span> »
  <span class="literal">In-Betweens</span> » <span class="literal">Blend
  To Neighbour</span>.
- Also available with the
  <span class="hotkeybg"><span class="hotkey">Alt</span><span class="hotkey">⇧
  Shift</span><span class="hotkey">E</span></span> shortcut.

<figure>
<video src="../../../videos/Blend_to_neighbour_2.mp4" width="600" controls=""></video>
</figure>
