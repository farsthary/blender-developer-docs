# Blender 3.2 Release Notes

Blender 3.2 was released on June 8, 2022.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-2/).

## [Animation & Rigging](animation_rigging.md)

## [Core](core.md)

## [EEVEE & Viewport](eevee.md)

## [Grease Pencil](grease_pencil.md)

## [Modeling](modeling.md)

## [Nodes & Physics](nodes_physics.md)

## [Pipeline, Assets & I/O](pipeline_assets_io.md)

## [Platform-Specific Changes](platforms.md)

## [Python API & Text Editor](python_api.md)

## [Render & Cycles](cycles.md)

## [Sculpt, Paint, Texture](sculpt.md)

## [User Interface](user_interface.md)

## [VFX & Video](vfx.md)

## [Virtual Reality](virtual_reality.md)

## [Add-ons](add_ons.md)

## Compatibility

### Proxy Removal

The proxy system has been fully removed, see [the Core release
notes](core.md) for details.

### Legacy Geometry Nodes

Geometry nodes from the pre-3.0 named attribute system have been
removed. See[ the geometry nodes release
notes](nodes_physics.md) for
details.

## [Corrective Releases](corrective_releases.md)
