# Lighting Bundle

A set of objects and world shaders to help quickly light a scene.

## IES

- Area
- Bollar
- Comet
- Jellyfish
- Medium Scatter
- Overhead
- Parallel Beam
- Pear
- Round
- Scatter
- Star Focused
- Three Lobe Umbrella
- Three Lobe Vee
- Tight Focused
- Top Post
- Umbrella
- X Arrow
- X Arrow Diffuse
- X Arrow Soft

## Future Plans

This bundle is planned to be expanded with Light Sets. Collection assets
are currently not supported though.

## Credits

The IES files were originated from [Pixar Public Domain
repository](https://renderman.pixar.com/ies-profiles).
