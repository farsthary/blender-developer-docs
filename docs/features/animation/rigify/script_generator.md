# UI Script Generator

Every generated rig comes with a Python script that implements a set of
rig-specific UI panels. The generation of this script is managed by the
ScriptGenerator [generator
plugin](generator.md#generatorplugin).

The script consists of a set of utility functions and operators,
followed by the panels themselves.

## ScriptGenerator

An instance of this class implements the generation of the script, and
is accessible as `generator.script` or `rig.script`.

### Utility Code

These methods are used to add utility code to the script, and register
it when necessary.

All string lists added by these methods are concatenated together and
filtered for duplicates before adding to the script. This allows easily
sharing code between rigs without duplication by sharing code strings.

`script.add_imports(str_list)`  
:   Adds module import lines to the script.

`script.add_utilities(str_list)`  
:   Add utility code to the script.

`script.register_classes(name_list)`  
:   Add class names to be registered in the module register function.

`script.register_driver_functions(name_list)`  
:   Add function names to be added to the driver function namespace.

`script.register_property(name, definition)`  
:   Add a custom property to be registered.

Examples:

``` Python
script.add_imports(['import math'])

script.add_utilities(['''
def test_add(a, b):
  return a + b
'''])

script.register_classes(['OBJECT_OP_rigify_test'])

script.register_driver_functions(['test_add'])

script.register_property('Object.test_prop', '''bpy.props.StringProperty(name="test")''')
```

It is recommended to store the string lists used in calls to these
functions in module global variables, in order to allow other modules to
easily share the code later by using the same globals.

### UI Panel Code

The script provides a UI panel that shows properties and operators
relevant for the currently selected bones of the rig. These methods are
used to generate the code for it.

`script.add_panel_code(str_list)`  
:   Directly adds code to the panel. This method is **deprecated** and
    provided only for compatibility with legacy code.

`script.panel_with_selected_check(rig, control_names)`  
:   Returns a PanelLayout object for generating a section of the panel
    belonging to the specified rig, and shown when the specified controls
    are selected. Repeated calls with the same parameters return the same
    panel object.
    
    The sub-panels are ordered based on the sequence of the calls to this
    method, with panels belonging to the same rig always placed adjacent to
    each other.

## PanelLayout

Objects of this class are used to generate the panel code in a
structured way, resembling the standard UILayout code.

The methods of the class accept arbitrary keyword parameters, denoted
with '`...`', which are converted to strings and added verbatim to the
generated UILayout calls.

`panel.script`  
:   Property referencing the ScriptGenerator that owns the panel.

`panel.use_bake_settings()`  
:   Request the Bake settings panel to be generated.

`panel.custom_prop(bone_name, prop_name, ...)`  
:   Generates a `layout.prop` call for the specified custom property of
    the specified bone.

`panel.operator(operator_name, properties=None, ...)`  
:   Generates a `layout.operator` call, invoking the specified operator.
    The `properties` dictionary may be provided to assign input values to
    the properties of the operator itself.
    
    The operator name may contain `'{rig_id}'` to automatically include
    the unique ID string of the rig.

`panel.row(...)`  
:   Generates a `layout.row` call, and returns a PanelLayout object
    representing the sub-panel.

`panel.column(...)`  
:   Generates a `layout.column` call, and returns a PanelLayout object
    representing the sub-panel.

`panel.split(...)`  
:   Generates a `layout.split` call, and returns a PanelLayout object
    representing the sub-panel.

### Sub-panel properties

`panel.active = value`  
:   Sets the `active` property of the sub-panel to the specified value.

`panel.enabled = value`  
:   Sets the `enabled` property of the sub-panel to the specified value.

### Expressions

The script generator supports using expressions that depend on bone
property values at runtime, and can be used in place of any value inputs
to the methods and properties above. Conversely, non-expression
constants can be used wherever an expression is expected in the method
below.

The expressions are represented by the PanelExpression class, and can be
initially created through a few of the methods of the panel. They also
implement many operators on their own.

`panel.expr_bone(bone_name)`  
:   Returns a reference expression representing the PoseBone with the
    specified name (can be an expression or constant).

`panel.expr_and(expressions...)`  
:   Returns a boolean and expression combining the expressions specified as
    parameters.

`panel.expr_or(expressions...)`  
:   Returns a boolean or expression combining the expressions specified as
    parameters.

`panel.expr_if_else(if_expr, true_expr, false_expr)`  
:   Returns a conditional expression combining the expressions specified as
    parameters.

`panel.expr_call(func_name: str, parameters...)`  
:   Returns a function call expression that invokes the specified function
    on the given parameter expressions.

## PanelExpression

This class represents an expression that would be evaluated at the
script runtime, rather than during creation.

It returns the expression string from its `repr()`, and also overrides
the following operators:

- Arithmetic: `+`, `-`, `*`, `/`, `//`, `%`
- Bitwise: `<<`, `>>`, `|`, `&`, `^`
- Unary: `+`, `-`, `~`
- Comparison: `<`, `<=`, `==`, `!=`, `>=`, `>`
- Functions: `abs()`, `round()`, `trunc()`, `floor()`,
  `ceil()`

These operators all accept either other expressions, or non-expression
constants as their second input.

The boolean and conditional operators cannot be overridden and thus are
implemented as functions of PanelLayout. To prevent mistakes, the
expression class overrides conversion to bool to throw an exception.

### Reference expressions

Expressions that wrap a direct reference, such as the result of
`panel.expr_bone()` also implement access operators:

`expr.field`  
:   Returns a reference expression that represents the specified field of
    the expression target.

`expr[item_expr]`  
:   Returns a reference expression that represents the specified item of the
    expression target.

`expr.get(item_expr, default=None)`  
:   Returns a reference expression that represents the specified item of the
    expression target, accessed via the `get()` method with default value.

### Examples

- `panel.expr_bone("foo")["bar"]` accesses the custom property
  `bar` of the bone `foo`.
- `panel.expr_bone("foo").get("bar", 0)` accesses the custom property
  `bar` of the bone `foo`, using the value 0 if it doesn't exist.
- `panel.expr_bone("foo").location[0]` accesses the X location
  property of the bone `foo`.
- `panel.enabled = panel.expr_bone("foo")["bar"] > 0` sets the
  enabled property of the panel based on comparing the value of the
  custom property with 0.
