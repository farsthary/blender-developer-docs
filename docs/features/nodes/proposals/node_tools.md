# Node Tools Prototype

## Pitch

These are the uses cases we tried to address.

- As an asset creator I want my assets to be easy to use so that people
  want to use them.
- As a set dresser I want to use assets without caring too much about
  what they do internally so that I can get my job done faster.

A potential solution is to let asset creators combine their node groups
with corresponding tools and ship them to users together. We call these
tools "node tools" (although sometimes they my be referred to as "asset
tools" below). We set out to create a simple prototype to get a better
idea of how the user experience could feel like.

## Prototype

The prototype allows a node group to expose names for vertex groups that
it wants to use and their default value.

![](../../../images/Node_tools_prototype_node_editor.png)

A new panel in the 3D view sidebar shows the inputs of a geometry node
group used by the active object. Those are the same settings that are
available in the modifier panel as well. Furthermore, it shows buttons
for all the different vertex groups that the node group would like to
use. Clicking on one of the buttons switches the object to weight paint
mode and sets up a weight group with the correct name. This way, the
user can start drawing immediately, without having to worry about
setting things up.

![](../../../images/Node_tools_in_3d_view.png)

See [T86395](https://developer.blender.org/T86395) for a video of how
the system can be used.

## Findings

Even though the prototype is very basic, it showed very well how
powerful this concept of attaching tools to node groups can be. It made
it significantly easier to use a geometry node group asset, because one
did not have to figure out how to setup everything correctly.

Some misc topics that came up during discussions:

- Should the buttons be part of the tool shelf on the left? It makes
  sense given the fact that they are "tools". However, there are also
  quite different from other tools we already have. For example,
  currently tools are mode specific, they don't just switch the mode an
  object is in. Also, existing tools can be enabled and disabled. It
  might not be possible to allow this for all kinds of tools.
  Especially, when a "tool" is just an operator that does some setup
  work on the underlying object.
- Is this idea of tool specific to node groups or can the concept be
  extended to more general "asset tools"?
- What other tools would be useful? Widgets that control certain node
  group inputs?
- How customizable should these tools be? Should it be possible to
  define a custom tool using Python? If yes, how would the Python code
  and the tool be packaged up? Should it just be an addon? How to deal
  with API breakages when Python code is part of an asset that is
  linked/appended into many projects?
- How does this fit into a greater attribute pipeline that spans from an
  attribute edit (paint) mode, over geometry nodes to rendering?
